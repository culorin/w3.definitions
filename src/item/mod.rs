//
// definitions::item (yaml) definitions
//
// ----------------------------------------------------------------------------
mod reader;
// mod writer;

use yaml::parser::Parser;
use {DatasetParser, Result, TryFromYaml};

use model::item::{ItemCategory, ItemDefinition, LootDefinition, RewardDefinition};
use model::primitives::references::ItemReference;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl DatasetParser for ItemDefinition {
    type Dataset = Vec<(String, ItemDefinition)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(_context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        debug!("> found items definition...");

        let mut items = Vec::new();
        for kvparser in data.as_map()?.iter() {
            let (id, value) = kvparser.to_key_value()?;
            let category = id.to_lowercase();

            for kvparser in value.as_map()?.iter() {
                let (id, value) = kvparser.to_key_value()?;

                let id = id.to_lowercase();
                let mut item = ItemDefinition::try_from_yaml(&id, &value)?;
                item.set_category(ItemCategory::from(category.as_str()));

                items.push((item.uid(), item));
            }
        }
        Ok(items)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetParser for LootDefinition {
    type Dataset = Vec<(String, LootDefinition)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(_context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        debug!("> found loot definition...");

        let mut lootdata = Vec::new();

        for kvparser in data.as_map()?.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let id = id.to_lowercase();
            let loot = LootDefinition::try_from_yaml(&id, &value)?;

            lootdata.push((id, loot));
        }
        Ok(lootdata)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetParser for RewardDefinition {
    type Dataset = Vec<(String, RewardDefinition)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(_context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        debug!("> found rewards definition...");

        let mut rewards = Vec::new();
        for kvparser in data.as_map()?.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let id = id.to_lowercase();
            let reward = RewardDefinition::try_from_yaml(&id, &value)?;

            rewards.push((id, reward));
        }
        Ok(rewards)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
