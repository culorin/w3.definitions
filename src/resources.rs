//
// definitions::resources
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::resources::{EncoderMetaData, GenericResource, QuestResources};

use model::ValidatableElement;
// ----------------------------------------------------------------------------
impl DatasetParser for QuestResources {
    type Dataset = Vec<(String, GenericResource)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found generic resources definitions...");

        let mut dataset = Vec::new();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;
            let id = id.to_lowercase();

            let mut resource = GenericResource::new(&id);

            // extract additional metadata
            if let Some(metadata) = value.as_map()?.get_value(".encoder") {
                resource.set_metadata(EncoderMetaData::try_from_yaml((), &metadata?)?);
            }
            if let Some(context) = context {
                resource.set_context(context);
            }
            resource.validate(&id)?;

            dataset.push((id, resource));
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for QuestResources {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if !data.is_empty() {
            debug!("> adding resources definition...");
            for (id, resource) in data {
                self.add_resource(id, resource)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for EncoderMetaData {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        trace!("> found encoder metada definition...");

        let mut meta = EncoderMetaData::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "targetname" => meta.set_targetname(value.to_lowercase_string()?),
                "targetpath" => meta.set_targetpath(value.to_lowercase_string()?),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        meta.validate(data.id()).and(Ok(meta))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
