//
// w3 quest definitions reader / writer
//
extern crate chrono;
#[macro_use]
extern crate log;
#[macro_use]
extern crate logger;
extern crate rayon;
extern crate uuid;
extern crate w3_model as model;
#[macro_use]
extern crate yaml_utils as yaml;
// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub mod quest;
pub mod repository;

pub use repository::{EntityMeshRegistry, Repository};

pub use self::repository::save_meshregistry_definition;
pub use self::shadowlayer::save_shadowlayer_definition;

pub use self::reader::{DatasetContainer, DatasetReader, StatsOutput};
// ----------------------------------------------------------------------------
type Result<T> = result::Result<T, std::string::String>;
// ----------------------------------------------------------------------------
mod reader;
mod writer;

mod settings;

mod community;
mod item;
mod journal;
mod layer;
mod navdata;
mod questgraph;
mod resources;
mod shadowlayer;

mod primitives;
mod rtti;
mod shared;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
// local converter for external types
// ----------------------------------------------------------------------------
trait TryConvertFrom<S, R = Self> {
    fn try_convert_from(src: S) -> Result<R>
    where
        R: Sized;
}
// ----------------------------------------------------------------------------
/// Trait for deserializing a data type struct from yaml
trait TryFromYaml<D, R = Self> {
    type Id;
    // ------------------------------------------------------------------------
    fn try_from_yaml(id: Self::Id, data: &D) -> Result<R>
    where
        R: Sized;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
/// Trait for deserializing a data type struct from yaml with additional info
trait TryFromYamlWithInfo<D, I, R = Self> {
    type Id;
    // ------------------------------------------------------------------------
    fn try_from_yaml(id: Self::Id, data: &D, info: &I) -> Result<R>
    where
        R: Sized;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
/// to ensure easy parallelization for definition loading data reading + parsing
/// and integrating are split into dedicated methods.
trait DatasetParser {
    type Dataset;
    // ------------------------------------------------------------------------
    fn parse_dataset(context: Option<&str>, data: &Parser) -> Result<Self::Dataset>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
/// many but not all packages are mergeable. this trait provides normalized api
trait DatasetIntegrator: DatasetParser {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
/// crate local trait for serializing structures (from model crate)
trait IntoYaml {
    type Output;

    #[allow(clippy::wrong_self_convention)]
    fn into_yaml(&self) -> Self::Output;
}
// ----------------------------------------------------------------------------
use std::result;

use yaml::parser::Parser;

use model::ValidatableElement;
// ----------------------------------------------------------------------------
// unsupported definition data parts are treated as rawdata (even if it is valid yml)
// for now. these helper functions convert between a "raw" string and the yml defintion
// ----------------------------------------------------------------------------
fn rawdata_to_string(data: &yaml::Yaml) -> Result<String> {
    use yaml::encoder::YamlEmitter;

    let mut data_str = String::new();

    let mut emitter = YamlEmitter::new(&mut data_str);
    emitter
        .dump(data)
        .map_err(|e| format!("rawdata deserializer: {:?}", e))?;

    Ok(data_str)
}
// ----------------------------------------------------------------------------
fn rawdata_to_yaml(data: &str) -> Result<Option<yaml::Yaml>> {
    yaml::parser::YamlLoader::load_from_str(data)
        .map(|mut list| list.pop())
        .map_err(|e| format!("rawdata deserializer: {:?}", e))
}
// ----------------------------------------------------------------------------
