//
// definitions::layer (yaml) definitions
//
// ----------------------------------------------------------------------------
mod reader;
// mod writer;

use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::layer::entities;
use model::layer::{LayerContent, QuestLayer, QuestLayers};
use model::ValidatableElement;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl DatasetParser for QuestLayers {
    type Dataset = Vec<(String, QuestLayer)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found quest layer definitions...");

        let mut dataset = Vec::new();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let id = id.to_lowercase();
            let mut layer = QuestLayer::try_from_yaml(&id, &value)?;

            if let QuestLayer::Data(ref mut layer) = layer {
                if let Some(context) = context {
                    layer.set_context(context);
                    layer
                        .validate("directory names in path")
                        .map_err(|e| format!("{}. found: {}", e, context))?;
                }
            }
            dataset.push((id, layer));
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for QuestLayers {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if !data.is_empty() {
            debug!("> adding layers definition...");
            for (id, layer) in data {
                self.add_layer(id, layer)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
