//
// layer::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use yaml::parser::{Parser, YamlType};

use model::primitives::{RotatedPosition, Rotation, Vector3D};

use {Result, TryFromYaml, ValidatableElement};

use super::entities::{
    ActionPoint, Area, EntitySpecialization, InteractiveEntity, MapPin, ScenePoint, StaticEntity,
    WanderPoint, WanderPointGroup, Waypoint,
};

use super::{LayerContent, QuestLayer};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for QuestLayer {
    type Id = &'data str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser<'data>) -> Result<Self> {
        use self::YamlType::*;
        // referencing a layer dir directly
        // vanilla_1: levels\<worldid>\<w2l-dir-path>
        // vanilla_2: <worldid>:dlc\ep1\[levels\novigrad\]<w2l-dir-path>

        // full definition:
        //   world: prologue
        //   entities: { ... }
        //   ...

        fn normalize_hubid(worldid: &str) -> std::string::String {
            // some (shortened) radish hub ids do match exactly the games hub id
            // normalize to rafdish hub ids
            match worldid.to_lowercase().as_str() {
                "prolog_village" | "prologue_village" => "prologue",
                "wyzima_castle" => "vizima",
                "island_of_mist" => "isle_of_mists",
                "the_spiral" => "spiral",
                "prologue_village_winter" | "prolog_village_winter" => "prologue_winter",
                as_is => as_is,
            }
            .to_owned()
        }

        let layer = match data.datatype() {
            String => {
                trace!("> found layer [{}] reference...", name);
                match data.as_str()? {
                    layerref if layerref.ends_with(".w2l") => {
                        return yaml_err!(
                            data.id(),
                            format!(
                                "layer reference must be a directory. extension indicates file: {}",
                                layerref
                            )
                        )
                    }
                    layerref => {
                        let path = layerref.replace('\\', "/").replace("//", "/");

                        let (world_prefix, path) = if let Some(pos) = path.find(':') {
                            let (world, path) = path.split_at(pos);
                            (Some(normalize_hubid(world)), path[1..].to_lowercase())
                        } else {
                            (None, path.to_lowercase())
                        };

                        // filter levels/<worldid>
                        let parts: Vec<&str> = path.split('/').collect();
                        let mut next_hubid = false;
                        let mut hubid = None;
                        let mut filtered_path = Vec::new();
                        for dir in parts {
                            match dir {
                                // skip levels dir
                                "levels" => {
                                    next_hubid = true;
                                }
                                _ if next_hubid => {
                                    next_hubid = false;
                                    hubid = Some(normalize_hubid(dir));
                                }
                                // skip empty dir
                                "" => {}
                                _ => filtered_path.push(dir),
                            }
                        }
                        let path = filtered_path.join("/");

                        let hubid = match (world_prefix, hubid) {
                            (None, Some(id)) => id,
                            (Some(id), None) => id,
                            (Some(prefix), Some(level)) => {
                                if prefix != level {
                                    return yaml_err!(
                                        data.id(),
                                        format!(
                                            "worldid info in layer reference is insonsistent: {}",
                                            layerref
                                        )
                                    );
                                }
                                level
                            }
                            (None, None) => {
                                return yaml_err!(
                                    data.id(),
                                    format!(
                                        "could not extract worldid from layer reference (full path \
                                        or <world>:<directory>). found: {}",
                                        layerref
                                    )
                                );
                            }
                        };
                        QuestLayer::FilePath(hubid, path)
                    }
                }
            }
            Hash => QuestLayer::Data(LayerContent::try_from_yaml(name.to_string(), data)?),

            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                    "supported types for layer definition are an unchecked layer reference string \
                    (full path or <world>:<directorypath>) or key value table. found: {}",
                    data.datatype_name()
                )
                )
            }
        };

        // layercontent will be validated on its own
        Ok(layer)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for LayerContent {
    type Id = String;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: String, data: &Parser<'data>) -> Result<Self> {
        trace!("> found layer [{}] definition...", name);

        let mut layer = LayerContent::new(name);

        // full layer definition:
        //   world: prologue
        //   staticEntities: { ... }
        //   ...

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "world" => layer.set_world(value.as_str()?),
                "context" => layer.set_context(value.as_str()?),

                "statics" | "staticentities" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(StaticEntity::try_from_yaml(name, &value)?);
                    }
                }
                "interactiveentities" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(InteractiveEntity::try_from_yaml(name, &value)?);
                    }
                }
                "mappins" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(MapPin::try_from_yaml(name, &value)?);
                    }
                }
                "areas" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(Area::try_from_yaml(name, &value)?);
                    }
                }
                "waypoints" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        let mut wpset = WaypointSet::try_from_yaml(name, &value)?;
                        for wp in wpset.0.drain(..) {
                            layer.add_entity(wp);
                        }
                    }
                }
                "scenepoints" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(ScenePoint::try_from_yaml(name, &value)?);
                    }
                }
                "actionpoints" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        let mut apset = ActionPointSet::try_from_yaml(name, &value)?;
                        for ap in apset.0.drain(..) {
                            layer.add_entity(ap);
                        }
                    }
                }
                "wanderpoints" => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        layer.add_entity(WanderPointGroup::try_from_yaml(name, &value)?);
                    }
                }
                specialization if specialization.contains('.') => {
                    for kvparser in value.as_map()?.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        let mut entity = StaticEntity::try_from_yaml(name, &value)?;
                        entity.set_specialization(EntitySpecialization::try_from(specialization)?);
                        layer.add_entity(entity);
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        layer.validate(data.id()).and(Ok(layer))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// layer entities
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for StaticEntity {
    type Id = &'data str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser<'data>) -> Result<Self> {
        let name = name.to_lowercase();

        debug!("> found static entity [{}] definition...", name);

        let layerentity = StaticEntity::new(name);

        // TODO

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for InteractiveEntity {
    type Id = &'data str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser<'data>) -> Result<Self> {
        let name = name.to_lowercase();

        debug!("> found interactive entity [{}] definition...", name);

        let layerentity = InteractiveEntity::new(name);

        // TODO

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for MapPin {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found mappin [{}] definition...", name);

        let mut layerentity = MapPin::new(name);
        // mappins:
        //   village: [258.0, 30.5, 5.0]
        //   village_2:
        //      # radius for quest instructions: 20
        //      pos: [158.0, 30.5, 5.0]

        match data.datatype() {
            YamlType::Array => layerentity.set_pos(Vector3D::try_from_yaml((), data)?),
            YamlType::Hash => {
                let map = data.as_map()?;
                for kvparser in map.iter() {
                    let (key, value) = kvparser.to_key_value()?;
                    match key.to_lowercase().as_str() {
                        "pos" => layerentity.set_pos(Vector3D::try_from_yaml((), &value)?),
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for mappin definition are list (of coordinates) or \
                         key-value table with \"pos\" setting. found: {}",
                        data.datatype_name()
                    )
                )
            }
        }

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Area {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found area [{}] definition...", name);

        let mut entity = Area::new(name);
        // square1:
        //   borderpoints:   # border point order matters! at least 3.
        //     - [338.0, 0.5, 5.0]
        //     - [340.0, 3.5, 5.0]
        //     - [330.0, 0.5, 5.0]
        //   height: 2.0
        // or:
        // area1:
        //   - [338.0, 0.5, 5.0]
        //   - [340.0, 3.5, 5.0]
        //   - [330.0, 0.5, 5.0]

        match data.datatype() {
            YamlType::Array => {
                let border = data.as_list()?;
                for point in border.iter() {
                    entity.add_borderpoint(Vector3D::try_from_yaml((), &point)?);
                }
            }
            YamlType::Hash => {
                let map = data.as_map()?;
                for kvparser in map.iter() {
                    let (key, value) = kvparser.to_key_value()?;
                    match key.to_lowercase().as_str() {
                        "borderpoints" => {
                            let border = value.as_list()?;
                            for point in border.iter() {
                                entity.add_borderpoint(Vector3D::try_from_yaml((), &point)?);
                            }
                        }
                        "height" => entity.set_height(value.to_float()?),

                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for area definition are list (of vectors) or \
                         key-value table. found: {}",
                        data.datatype_name()
                    )
                )
            }
        }

        entity
            .validate(data.id())
            .and(entity.finalize())
            .and(Ok(entity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Waypoint {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found waypoint [{}] definition...", name);

        let mut layerentity = Waypoint::new(name);
        // child_sleep:         # <dlcid>_<worldid>_<layer>_<name>_wp
        //   pos: [1.0, 2.0, 3.0]
        //   rot: [0.0, 0.0, 180.0]
        // or:
        // woman_sleep: [1.0, 2.0, 3.0, 180.0]

        layerentity.set_placement(RotatedPosition::try_from_yaml("waypoint", data)?.into());

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ScenePoint {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found scenepoint [{}] definition...", name);

        let mut layerentity = ScenePoint::new(name);

        // child_sleep:         # <dlcid>_<worldid>_<layer>_<name>_wp
        //   pos: [1.0, 2.0, 3.0]
        //   rot: [0.0, 0.0, 180.0]
        // or:
        // woman_sleep: [1.0, 2.0, 3.0, 180.0]

        layerentity.set_placement(RotatedPosition::try_from_yaml("scenepoint", data)?.into());

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn parse_action<'a>(data: &'a Parser) -> Result<(&'a str, &'a str)> {
    let action = data.as_str()?;
    let parts: Vec<_> = action.split('/').collect();
    match parts.len() {
        2 => Ok((parts[0], parts[1])),
        _ => yaml_err!(
            data.id(),
            format!(
                "expected \"<category>/<action>\" for action definition. found: {}",
                action
            )
        ),
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ActionPoint {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found actionpoint [{}] definition...", name);

        let mut layerentity = ActionPoint::new(name);

        // child_sleep:         # <dlcid>_<worldid>_<layer>_<name>_ap
        //   tags: [rain_shelter_man, rain_shelter_woman]
        //   pos: [1.0, 2.0, 3.0]
        //   rot: [0.0, 0.0, 180.0]
        //   action: resting_child/lie_mwdc_sleep_bed_r
        //   breakable: false
        //   ignoreCollisions: false
        //   disableSoftReactions: false
        //   firesourceDependent: false
        //   keepIkActive: false
        // or:
        // woman_sleep: [1.0, 2.0, 3.0, 180.0, sleeping_woman/lie_mwdc_sleep_bed_r]

        match data.datatype() {
            YamlType::Array => {
                let list = data.as_list()?;
                let list = list.as_parser_list();

                if list.len() != 5 {
                    return yaml_err!(
                        data.id(),
                        format!(
                            "expected a 5 elements list (x, y, z, z-rotation, category/action). \
                             found: {} elements",
                            list.len()
                        )
                    );
                }
                layerentity.set_pos(Vector3D(
                    list[0].to_float()?,
                    list[1].to_float()?,
                    list[2].to_float()?,
                ));
                layerentity.set_rot(Rotation(
                    0.0,
                    0.0,
                    list[3].to_float()?, // only roll supported
                ));

                let (category, action) = parse_action(&list[4])?;

                layerentity.set_action(category, action);
            }
            YamlType::Hash => {
                let map = data.as_map()?;
                for kvparser in map.iter() {
                    let (key, value) = kvparser.to_key_value()?;
                    match key.replace('_', "").to_lowercase().as_str() {
                        "tags" => layerentity.set_tags(value.to_list_of_lowercase_strings()?),
                        "pos" => layerentity.set_pos(Vector3D::try_from_yaml((), &value)?),
                        "rot" => layerentity.set_rot(Rotation::try_from_yaml((), &value)?),
                        "action" => {
                            let (category, action) = parse_action(&value)?;
                            layerentity.set_action(category, action);
                        }
                        "breakable" => layerentity.set_breakable(value.to_bool()?),
                        "ignorecollisions" => layerentity.set_collision_aware(!value.to_bool()?),
                        "disablesoftreactions" => layerentity.set_soft_reactions(!value.to_bool()?),
                        "firesourcedependent" => {
                            layerentity.set_firesource_dependent(value.to_bool()?)
                        }
                        "keepikactive" => layerentity.set_ik_active(value.to_bool()?),

                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for actionpoint definition are list or key-value table. \
                         found: {}",
                        data.datatype_name()
                    )
                )
            }
        }

        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WanderPointGroup {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let name = name.to_lowercase();

        trace!(">> found wanderpointgroup [{}] definition...", name);

        let mut group = WanderPointGroup::new(name);
        // simple_open_bidirectional_path:
        //   - [ 313.7657775879, 52.8895530701, 0.0972428322, 4.0 ]
        //   - [ 313.7657775879, 52.8895530701, 0.0972428322 ]
        //   - [ 313.7657775879, 52.8895530701, 0.0972428322, 2.0 ]
        //   - [ 313.7657775879, 52.8895530701, 0.0972428322, 2.0 ]
        let list = data.as_list()?;

        for point in list.iter() {
            group.add_point(WanderPoint::try_from_yaml((), &point)?);
        }
        group.connect();

        group.validate(data.id()).and(Ok(group))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WanderPoint {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let mut layerentity = WanderPoint::default();
        // [ 313.7657775879, 52.8895530701, 0.0972428322, 4.0 ]
        //   or
        // [ 313.7657775879, 52.8895530701, 0.0972428322 ]

        let list = data.to_list_of_floats()?;

        match list.len() {
            3 => layerentity.set_pos(Vector3D(list[0], list[1], list[2])),
            4 => {
                layerentity.set_pos(Vector3D(list[0], list[1], list[2]));
                layerentity.set_pointradius(list[3]);
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "expected a 3 or 4 elements list (x, y, z and optional wander radius). \
                         found: {} elements",
                        list.len()
                    )
                )
            }
        }
        layerentity.validate(data.id()).and(Ok(layerentity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// grouped entity definitions
// ----------------------------------------------------------------------------
macro_rules! try_from_yaml_for_entityset {
    ($setclass:ident, $entityclass:ident, $entitytype:expr) => {
        impl<'a> TryFromYaml<Parser<'a>> for $setclass {
            type Id = &'a str;
            // ------------------------------------------------------------------------
            fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
                let name = name.to_lowercase();

                trace!(">> found {} [{}] definition...", $entitytype, name);

                let mut collection = $setclass::default();

                match data.datatype() {
                    YamlType::Array => {
                        // two variants:
                        //  1. list of entity definitions
                        //  2. inline list for a single entity definition
                        let list = data.as_list()?;
                        let list = list.as_parser_list();
                        match list[0].datatype() {
                            YamlType::Array | YamlType::Hash => {
                                for def in &list {
                                    collection.0.push($entityclass::try_from_yaml(&name, def)?);
                                }
                            }
                            _ => collection
                                .0
                                .push($entityclass::try_from_yaml(&name, &data)?),
                        }
                    }
                    _ => collection
                        .0
                        .push($entityclass::try_from_yaml(&name, &data)?),
                }

                Ok(collection)
            }
            // ------------------------------------------------------------------------
        }
    };
}
// ----------------------------------------------------------------------------
#[derive(Default)]
struct WaypointSet(Vec<Waypoint>);
#[derive(Default)]
struct ActionPointSet(Vec<ActionPoint>);
// ----------------------------------------------------------------------------
try_from_yaml_for_entityset!(WaypointSet, Waypoint, "waypoint");
try_from_yaml_for_entityset!(ActionPointSet, ActionPoint, "actionpoint");
// ----------------------------------------------------------------------------
