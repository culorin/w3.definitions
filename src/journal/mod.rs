//
// definitions::journal journals (yaml) defintions
//
// ----------------------------------------------------------------------------
mod reader;
// mod writer;

use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::journal::{
    CharacterJournal, CreatureJournal, QuestJournal, QuestJournalMapPin, QuestJournalObjective,
    QuestJournalPhase, QuestJournals,
};

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
#[derive(Default)]
pub(super) struct JournalDataset {
    characters: Vec<(String, CharacterJournal)>,
    creatures: Vec<(String, CreatureJournal)>,
    quests: Vec<(String, QuestJournal)>,
}
// ----------------------------------------------------------------------------
impl DatasetParser for QuestJournals {
    type Dataset = JournalDataset;
    // ------------------------------------------------------------------------
    fn parse_dataset(_context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found quest journal definitions...");

        let mut dataset = JournalDataset::default();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (journaltype, value) = kvparser.to_key_value()?;

            match journaltype.to_lowercase().as_str() {
                "characters" => dataset.characters = parse_characters(&value)?,
                "creatures" => dataset.creatures = parse_creature(&value)?,
                "quests" => dataset.quests = parse_quest(&value)?,
                _ => return yaml_err_unknown_key!(data.id(), journaltype),
            }
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for QuestJournals {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if !data.characters.is_empty() || !data.creatures.is_empty() || !data.quests.is_empty() {
            debug!("> adding journals definition...");

            for (id, journal) in data.characters {
                self.add_character(id, journal)?;
            }
            for (id, journal) in data.creatures {
                self.add_creature(id, journal)?;
            }
            for (id, journal) in data.quests {
                self.add_quest(id, journal)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn parse_characters(data: &Parser) -> Result<Vec<(String, CharacterJournal)>> {
    debug!("> found character journal definition...");

    let mut dataset = Vec::new();
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (characterid, value) = kvparser.to_key_value()?;

        let id = characterid.to_lowercase();
        let journal = CharacterJournal::try_from_yaml(&id, &value)?;

        dataset.push((id, journal));
    }
    Ok(dataset)
}
// ----------------------------------------------------------------------------
fn parse_creature(data: &Parser) -> Result<Vec<(String, CreatureJournal)>> {
    debug!("> found creature journal definition...");

    let mut dataset = Vec::new();
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (creatureid, value) = kvparser.to_key_value()?;

        let id = creatureid.to_lowercase();
        let journal = CreatureJournal::try_from_yaml(&id, &value)?;

        dataset.push((id, journal));
    }
    Ok(dataset)
}
// ----------------------------------------------------------------------------
fn parse_quest(data: &Parser) -> Result<Vec<(String, QuestJournal)>> {
    debug!("> found quest journal definition...");

    let mut dataset = Vec::new();
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (questid, value) = kvparser.to_key_value()?;

        let id = questid.to_lowercase();
        let journal = QuestJournal::try_from_yaml(&id, &value)?;

        dataset.push((id, journal));
    }
    Ok(dataset)
}
// ----------------------------------------------------------------------------
