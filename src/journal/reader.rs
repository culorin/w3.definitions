//
// journal::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use super::{
    CharacterJournal, CreatureJournal, QuestJournal, QuestJournalMapPin, QuestJournalObjective,
    QuestJournalPhase,
};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for CharacterJournal {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found character journal [{}] definition...", name);

        let mut journal = CharacterJournal::new(name);
        // characterid:
        //   name: Some Name
        //   image: some.png
        //   group: secondary
        //   description:
        //     - main: "Some Text"
        //     - first: "lala"
        //     - alternative1: "lala1"

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "name" => journal.set_name(value.as_str()?),
                "image" => journal.set_image(value.as_str()?),
                "group" => journal.set_group(value.to_lowercase_string()?),
                "description" => {
                    for entry in value.as_list()?.iter() {
                        let entry = entry.as_map()?;
                        let (id, text) = entry.as_parser_pair()?;
                        journal.add_description(id.as_str()?, text.as_str()?);
                    }
                }

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        journal.validate(data.id()).and(Ok(journal))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for CreatureJournal {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found creature journal [{}] definition...", name);

        let mut journal = CreatureJournal::new(name);
        // shaiwhalud:
        //   name: thingy
        //   image: unknown.jpg
        //   group: relicts
        //   description:
        //     - main: "Mysterious creature"
        //   items:
        //     - water
        //     - earth

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "name" => journal.set_name(value.as_str()?),
                "image" => journal.set_image(value.as_str()?),
                "group" => journal.set_group(value.to_lowercase_string()?),
                "description" => {
                    for entry in value.as_list()?.iter() {
                        let entry = entry.as_map()?;
                        let (id, text) = entry.as_parser_pair()?;
                        journal.add_description(id.as_str()?, text.as_str()?);
                    }
                }
                "items" => journal.set_items(value.to_list_of_strings()?),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        journal.validate(data.id()).and(Ok(journal))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Helper for trait impl
struct QuestType<'a>(&'a str);
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestType<'a> {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.to_lowercase_string()?.as_str() {
            "story" => Ok(QuestType("Story")),
            "chapter" | "main" | "mainquest" => Ok(QuestType("Chapter")),
            "side" | "sidequest" => Ok(QuestType("Side")),
            "monsterhunt" => Ok(QuestType("MonsterHunt")),
            "treasurehunt" => Ok(QuestType("TreasureHunt")),
            unknown => yaml_err!(
                data.id(),
                format!(
                    "valid settings for type are \"MainQuest\", \"SideQuest\", \
                     \"MonsterHunt\" or \"TreasureHunt\". found: {}",
                    unknown
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestJournal {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found quest journal [{}] definition...", name);

        let mut journal = QuestJournal::new(name);
        // questid:
        //   title: "a quest name"
        //   type: mOnsterhunt
        //   world: noviGrad       # optional -> multiple worlds
        //   level: 1              # optional
        //   description:
        //     - start: Big beast. Go destroy
        //   instructions:
        //     phaseA:
        //          ...
        //     phaseKilled:
        //          ...;
        let mut world = None;

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "title" => journal.set_title(value.as_str()?),
                "type" => journal.set_questtype(QuestType::try_from_yaml((), &value)?.0),
                "world" => world = Some(value.to_lowercase_string()?),
                "level" => journal.set_level(value.to_integer()?.max(0).min(99) as u8),
                "description" => {
                    for entry in value.as_list()?.iter() {
                        let entry = entry.as_map()?;
                        let (id, text) = entry.as_parser_pair()?;
                        journal.add_description(id.as_str()?, text.as_str()?);
                    }
                }
                "instructions" => {
                    for kvparser in value.as_map()?.iter() {
                        let (phaseid, data) = kvparser.to_key_value()?;

                        journal
                            .add_phase(phaseid, QuestJournalPhase::try_from_yaml(phaseid, &data)?);
                    }
                }

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        // deferred world setting so it can be injected into all objectives
        if let Some(ref world) = world {
            journal.set_world(world, data.id())?;
        }

        journal.validate(data.id()).and(Ok(journal))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestJournalPhase {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        trace!(">> found quest phase [{}] definition...", name);
        let mut phase = QuestJournalPhase::new(name);
        // phaseA:
        //   - kill_monster: Kill it with igni
        //   - report_back:
        //       caption: Go collect reward
        //       mappins: somewhere/questgiver

        for definition in data.as_list()?.iter() {
            let definition = definition.as_map()?;
            let (objectiveid, objective) = definition.as_key_value()?;

            let objective = match objective.datatype() {
                String => QuestJournalObjective::new_with_caption(objectiveid, objective.as_str()?),
                Hash => QuestJournalObjective::try_from_yaml(objectiveid, &objective)?,
                _ => {
                    return yaml_err!(
                        definition.id(),
                        format!(
                        "supported types for objective definition are string or key value table. \
                        found: {}", objective.datatype_name())
                    )
                }
            };
            phase.add_objective(objectiveid, objective);
        }

        phase.validate(data.id()).and(Ok(phase))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestJournalObjective {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        let mut objective = QuestJournalObjective::new(name);
        // - search:
        //     world: skellige
        //     caption: Search Monster
        //     mappins:
        //       - somewhere/area1
        //       - [somewhere/area2, 20]
        // or
        // - report_back:
        //     caption: Go collect reward
        //     mappins: somewhere/questgiver

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "world" => objective.set_world(value.as_str()?),
                "caption" => objective.set_caption(value.as_str()?),
                "mappins" => match value.datatype() {
                    String => objective.add_mappin(QuestJournalMapPin::new(value.as_str()?)),
                    Array => {
                        for definition in value.as_list()?.iter() {
                            objective.add_mappin(QuestJournalMapPin::try_from_yaml((), &definition)?);
                        }
                    },
                    _ => return yaml_err!(value.id(), format!(
                        "supported types for objective mappins definition are string (mappin id) or \
                        list of mappin definitions. found: {}", value.datatype_name())),
                },

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        objective.validate(data.id()).and(Ok(objective))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestJournalMapPin {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        // mappins:
        //   - somewhere/area1
        //   - [somewhere/area2, 20]

        let mappin = match data.datatype() {
            String => QuestJournalMapPin::new(data.as_str()?),

            Array => {
                let data = data.as_list()?;
                let list = data.as_parser_list();
                if list.len() != 2 {
                    return yaml_err!(
                        data.id(),
                        format!(
                            "expected two element list (mappin id, radius). \
                             found: {} elements",
                            list.len()
                        )
                    );
                }
                let mut pin = QuestJournalMapPin::new(list[0].as_str()?);
                pin.set_radius(list[1].to_float()?);
                pin
            }

            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for mappin definition are string (mappin id) or \
                         two element list (mappin id, radius). found: {}",
                        data.datatype_name()
                    )
                )
            }
        };

        mappin.validate(data.id()).and(Ok(mappin))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
