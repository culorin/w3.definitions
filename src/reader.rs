//
// definitions::reader
//
extern crate glob;

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub trait StatsOutput {
    fn log_stats(&self);
}
// ----------------------------------------------------------------------------
pub trait DatasetContainer<S> {
    // ------------------------------------------------------------------------
    type Dataset: Send;
    // ------------------------------------------------------------------------
    fn parse(
        basedir: &Path,
        filepath: &Path,
        data: &Yaml,
        data_specs: &S,
    ) -> Result<Self::Dataset, String>;
    // ------------------------------------------------------------------------
    fn add(&mut self, data: Self::Dataset) -> Result<(), String>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub struct DatasetReader<T, S> {
    basedir: PathBuf,
    wildcard: String,
    _phantom: PhantomData<T>,
    _data_specs: PhantomData<S>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::marker::PhantomData;
use std::path::{Path, PathBuf};

use self::glob::glob;

use super::yaml::{Yaml, YamlLoader};
// ----------------------------------------------------------------------------
impl<T, S> YamlLoader for DatasetReader<T, S> {}
// ----------------------------------------------------------------------------
impl<T, S: Sync> DatasetReader<T, S>
where
    T: DatasetContainer<S>,
{
    // ------------------------------------------------------------------------
    pub fn new(inputpath: &Path, wildcard: &str) -> Result<DatasetReader<T, S>, String> {
        let input = inputpath
            .to_str()
            .unwrap_or_else(|| fatal!("path to string conversion failed"));

        let (basedir, wildcard) = if inputpath.is_dir() {
            (inputpath.to_owned(), format!("{}/**/{}", input, wildcard))
        } else {
            // split basedir and relative path based on current directory
            let mut current = inputpath;
            while let Some(parent) = current.parent() {
                current = parent;
                if current.is_dir() {
                    break;
                }
            }
            if !current.is_dir() {
                return Err(format!(
                    "failed to find a valid parent directory for: {}",
                    inputpath.display()
                ));
            }
            (current.to_owned(), input.to_string())
        };
        Ok(Self {
            basedir,
            wildcard,
            _phantom: PhantomData,
            _data_specs: PhantomData,
        })
    }
    // ------------------------------------------------------------------------
    pub fn wildcard(&self) -> &str {
        &self.wildcard
    }
    // ------------------------------------------------------------------------
    pub fn readdata(&self, mut repo: T, data_specs: &S) -> Result<T, String> {
        use rayon::prelude::*;

        // first collect paths from glob (doesn't support par_iter)
        let paths = glob(&self.wildcard)
            .unwrap()
            .flat_map(|entry| match entry {
                Ok(path) => Some(path),
                Err(e) => {
                    warn!("could not read {:?}. skipping.", e);
                    None
                }
            })
            .map(|path| {
                // provide relative directory to dataset
                path.strip_prefix(&self.basedir.strip_prefix(".").unwrap_or(&self.basedir))
                    .map(|relative_path| {
                        (path.to_string_lossy().to_string(), relative_path.to_owned())
                    })
                    .map_err(|e| {
                        format!(
                            "failed to extract relative dir for {}: {}",
                            path.display(),
                            e
                        )
                    })
            })
            .collect::<Result<Vec<_>, String>>()?;

        // help borrow checker
        let basedir = &self.basedir;

        let mut datasets = paths
            .par_iter()
            .enumerate()
            .map(|(i, (path, relative_path))| match Self::read_yaml(path) {
                Ok(ref data) => {
                    trace!("found dataset definitions");
                    T::parse(basedir, relative_path, data, data_specs).map(|dataset| (i, dataset))
                }
                Err(why) => Err(format!("could not read dataset file {}: {}", &path, why)),
            })
            .collect::<Result<Vec<_>, String>>()?;

        // sort by processing order (this may be not necessary if rayon keeps order of collected ?)
        datasets.sort_by_key(|data| data.0);

        // integrate data into repository
        for (_, dataset) in datasets.drain(..) {
            repo.add(dataset)?;
        }

        Ok(repo)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
