//
// shadowmesh::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::path::Path;

use yaml::parser::{MapParser, Parser};
use yaml::Yaml;
use {Result, TryFromYaml, ValidatableElement};

use model::{
    primitives::{Rotation, Transform3D, Vector3D},
    shadowlayer::{
        ShadowLayer, ShadowLayers, ShadowLayersDefinition, ShadowMeshEntity, ShadowMeshInfo,
    },
};

use repository::Repository;

use reader::{DatasetContainer, StatsOutput};
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct ShadowLayersDataset {
    modid: String,
    hublayers: Vec<(String, ShadowLayers)>,
}
// ----------------------------------------------------------------------------
impl DatasetContainer<Repository> for ShadowLayersDefinition {
    type Dataset = ShadowLayersDataset;
    // ------------------------------------------------------------------------
    fn parse(
        _basedir: &Path,
        _filepath: &Path,
        data: &Yaml,
        repo: &Repository,
    ) -> Result<Self::Dataset> {
        use std::collections::HashSet;

        debug!("parsing shadowmesh definition content...");

        let mut dataset = ShadowLayersDataset::default();
        let mut found_hubs = HashSet::new();

        let data = MapParser::new("shadowmesh definition", data)?;

        for kvparser in data.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "layers.shadow" => {
                    let data = value.as_map()?;
                    let modid = data
                        .get_value(".modid")
                        .ok_or(yaml_errmsg!(
                            data.id(),
                            "required \".modid\" setting not found"
                        ))??
                        .to_lowercase_string()?;
                    let hubid = data
                        .get_value(".world")
                        .ok_or(yaml_errmsg!(
                            data.id(),
                            "required \".world\" setting not found"
                        ))??
                        .to_lowercase_string()?;

                    if !repo.worlds().contains_key(&hubid) {
                        return yaml_err!(data.id(), format!("found unknown world id: {}", hubid));
                    }

                    dataset.modid = modid;

                    let mut shadow_layers = ShadowLayers::default();

                    for kvparser in data.iter() {
                        let (key, value) = kvparser.to_key_value()?;

                        match key.to_lowercase().as_str() {
                            ".modid" | ".world" => {
                                // ignore the settings already loaded
                            }
                            tileid => {
                                shadow_layers
                                    .add_layer(tileid, ShadowLayer::try_from_yaml(key, &value)?)?;
                            }
                        }
                    }
                    if found_hubs.contains(&hubid) {
                        return yaml_err!(
                            data.id(),
                            format!("found duplicate shadowlayer definition for hub: {}", hubid)
                        );
                    } else {
                        found_hubs.insert(hubid.clone());
                        dataset.hublayers.push((hubid, shadow_layers));
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
    fn add(&mut self, data: Self::Dataset) -> Result<()> {
        for (hubid, shadow_layers) in data.hublayers {
            self.add_hublayer(hubid, shadow_layers)?;
        }
        self.set_id(data.modid);

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl StatsOutput for ShadowLayersDefinition {
    // ------------------------------------------------------------------------
    fn log_stats(&self) {
        if !self.is_empty() {
            info!("found shadowmesh definitions:");
            for (hubname, tiles) in self.hublayers() {
                info!(" {}: #{} layers", hubname, tiles.layers().len());
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ShadowLayer {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found shadowmesh layer definition [{}]...", name);

        let mut layer = ShadowLayer::default();

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            layer.add_entity(key, ShadowMeshEntity::try_from_yaml(key, &value)?)?;
        }

        layer.validate(data.id()).and(Ok(layer))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ShadowMeshEntity {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found shadowmesh entity definition [{}]...", name);

        let mut entity = ShadowMeshEntity::new(
            name,
            Vector3D::default(),
            Rotation::default(),
            Vector3D::from((1.0, 1.0, 1.0)),
        );

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "transform" => entity.set_transform(Transform3D::try_from_yaml((), &value)?),
                "meshes" => {
                    let mesh_list = value.as_list()?;

                    for value in mesh_list.iter() {
                        let mesh = ShadowMeshInfo::try_from_yaml(value.id(), &value)?;
                        entity.add_mesh(mesh);
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        entity.validate(data.id()).and(Ok(entity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ShadowMeshInfo {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(_name: &str, data: &Parser) -> Result<Self> {
        let mut mesh = ShadowMeshInfo::default();

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "src" => mesh.set_srcmesh(value.to_lowercase_string()?),
                "shadowmesh" => mesh.set_mesh(value.to_lowercase_string()?),
                "transform" => mesh.set_transform(Transform3D::try_from_yaml((), &value)?),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        mesh.validate(data.id()).and(Ok(mesh))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
