//
// questgraph::conditions::reader - quest block conditions (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// timecondition type helper
pub(crate) enum TimeConditionType {
    Before,
    After,
    Range,
    Elapsed,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use model::primitives::references::{InteractionReference, LayerTagReference};
use model::primitives::{HiResTime, Time};

use model::questgraph::{EditorBlockData, QuestBlock};

use model::questgraph::conditions::{Condition, WaitUntil};
use model::questgraph::conditions::{
    EnteredLeftAreaCondition, InsideOutsideAreaCondition, InteractionCondition, InteractionType,
    LoadingScreenCondition, LogicCondition, TimeCondition,
};
use model::shared::{FactsDbCondition, LogicOperation};

use super::common::*;
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WaitUntil {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found waituntil-block [{}] definition...", name);

        let mut block = WaitUntil::new(name);

        // waituntil.fact:
        //   factdb: [ somefactid, "=", 1 ]
        //   next:                          # must be next[.Out] for one condition
        //     - script.do_something

        // # multiple conditions possible, "name" attribute of condition maps to out socket (1:1 relation)
        // waituntil.something:
        //   conditions:
        //     cond1:                      # name which has to map outsocketid (here: -> next.cond1)
        //       factdb: [ somefactid_cond1, "<", 1 ]
        //     cond2:
        //       factdb: [ somefactid_cond2, ">", 1 ]
        //   next.cond1: script.do_something
        //   next.cond2: scene.somename2

        let definition = data.as_map()?;
        let mut condition_found = false;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(true, &value)?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                "conditions" if !condition_found => {
                    condition_found = true;
                    let condition_set = value.as_map()?;

                    for named_condition in condition_set.iter() {
                        let (name, condition) = named_condition.to_key_value()?;
                        let condition = condition.as_map()?;

                        let (cond_typeid, cond_def) = condition.as_key_value()?;

                        block.add_condition(
                            Some(name),
                            Condition::try_from_yaml(cond_typeid, &cond_def)?,
                        );
                    }
                }
                inline_cond_typeid if !condition_found => {
                    condition_found = true;
                    block
                        .add_condition(None, Condition::try_from_yaml(inline_cond_typeid, &value)?);
                }
                _ => {
                    return if condition_found {
                        yaml_err!(format!(
                            "{}: only one inline-condition or a \"conditions\"-table allowed.",
                            data.id()
                        ))
                    } else {
                        yaml_err_unknown_key!(data.id(), key)
                    }
                }
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// conditions
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Condition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(condition_type: &str, data: &Parser) -> Result<Self> {
        // trace!(">> found condition [{}] definition...", name);

        match condition_type.to_lowercase().as_str() {
            "factdb" => Ok(Condition::FactsDb(FactsDbCondition::try_from_yaml(
                (),
                data,
            )?)),

            // logic operator for multiple conditions
            "all" | "every" | "and" => Ok(Condition::Logic(LogicCondition::try_from_yaml(
                LogicOperation::AND,
                data,
            )?)),
            "any" | "or" => Ok(Condition::Logic(LogicCondition::try_from_yaml(
                LogicOperation::OR,
                data,
            )?)),
            "exactlyone" | "onlyone" | "xor" => Ok(Condition::Logic(
                LogicCondition::try_from_yaml(LogicOperation::XOR, data)?,
            )),
            "notall" | "nand" => Ok(Condition::Logic(LogicCondition::try_from_yaml(
                LogicOperation::NAND,
                data,
            )?)),
            "none" | "nor" => Ok(Condition::Logic(LogicCondition::try_from_yaml(
                LogicOperation::NOR,
                data,
            )?)),
            "nxor" => Ok(Condition::Logic(LogicCondition::try_from_yaml(
                LogicOperation::NXOR,
                data,
            )?)),

            // trigger areas
            "entered" => Ok(Condition::EnteredLeftArea(
                EnteredLeftAreaCondition::try_from_yaml(true, data)?,
            )),
            "left" => Ok(Condition::EnteredLeftArea(
                EnteredLeftAreaCondition::try_from_yaml(false, data)?,
            )),
            "inside" => Ok(Condition::InsideOutsideArea(
                InsideOutsideAreaCondition::try_from_yaml(true, data)?,
            )),
            "outside" => Ok(Condition::InsideOutsideArea(
                InsideOutsideAreaCondition::try_from_yaml(false, data)?,
            )),

            // time
            "time" => Ok(Condition::Time(TimeCondition::try_from_yaml(
                TimeConditionType::Range,
                data,
            )?)),
            "after" => Ok(Condition::Time(TimeCondition::try_from_yaml(
                TimeConditionType::After,
                data,
            )?)),
            "before" => Ok(Condition::Time(TimeCondition::try_from_yaml(
                TimeConditionType::Before,
                data,
            )?)),
            "elapsed" => Ok(Condition::Time(TimeCondition::try_from_yaml(
                TimeConditionType::Elapsed,
                data,
            )?)),

            // interactions
            "interaction" => Ok(Condition::Interaction(InteractionCondition::try_from_yaml(
                None, data,
            )?)),
            "examined" => Ok(Condition::Interaction(InteractionCondition::try_from_yaml(
                Some(InteractionType::Examine),
                data,
            )?)),
            "used" => Ok(Condition::Interaction(InteractionCondition::try_from_yaml(
                Some(InteractionType::Use),
                data,
            )?)),
            "talked" => Ok(Condition::Interaction(InteractionCondition::try_from_yaml(
                Some(InteractionType::Talk),
                data,
            )?)),
            "looted" => Ok(Condition::Interaction(InteractionCondition::try_from_yaml(
                Some(InteractionType::Loot),
                data,
            )?)),

            // loadingscreen
            "loadscreen" => Ok(Condition::LoadingScreen(
                LoadingScreenCondition::try_from_yaml((), data)?,
            )),

            unknown => yaml_err!(format!(
                "{}: found unknown condition type: {}",
                data.id(),
                unknown
            )),
        }
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LogicCondition {
    type Id = LogicOperation;
    // ------------------------------------------------------------------------
    fn try_from_yaml(operation: LogicOperation, data: &Parser) -> Result<Self> {
        trace!(
            ">> found logic-condition-operator [{}] definition...",
            operation
        );

        let mut op = LogicCondition::new(operation);
        // any:
        //   - factdb: [ fact1, "=", 1 ]
        //   - factdb: [ fact2, "=", 1 ]
        //   - factdb: [ fact3, "=", 1 ]

        let conditions_list = data.as_list()?;

        for parser in conditions_list.iter() {
            let map = parser.as_map()?;
            let (condition_type, value) = map.as_key_value()?;

            op.add_condition(Condition::try_from_yaml(condition_type, &value)?);
        }

        op.validate(data.id()).and(Ok(op))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn extract_area_tag(data: &Parser) -> Result<(Option<String>, LayerTagReference)> {
    // entered:
    //   who: PLAYER            # optional, default player, case sensitive
    //   area: area2
    //   world: prologue        # required only if area is not defined with worldid: <worldid>/<areaid>
    //
    // or:
    // entered: prologue/area2
    //
    // or:
    // entered: [prologue/area2, some_actor_tag]

    match data.datatype() {
        YamlType::String => Ok((None, LayerTagReference::try_from_yaml((), data)?)),
        YamlType::Array => {
            let list = data.as_list()?;
            let list = list.as_parser_list();
            match list.len() {
                2 => {
                    let area = LayerTagReference::try_from_yaml((), &list[0])?; //parse_area_tag(data.id(), &list[0])?;
                    let actortag = list[1].to_string()?;
                    Ok((Some(actortag), area))
                }
                _ => yaml_err!(
                    data.id(),
                    format!(
                        "expected two element string list [<area>, <actortag>]. \
                         found: {} elements",
                        list.len()
                    )
                ),
            }
        }
        YamlType::Hash => {
            let definition = data.as_map()?;

            let mut world = None;
            let mut actortag = None;
            let mut area = String::default();

            for kvparser in definition.iter() {
                let (key, value) = kvparser.to_key_value()?;

                match key.to_lowercase().as_str() {
                    "who" => actortag = Some(value.to_string()?),
                    "area" => area = value.to_lowercase_string()?,
                    "world" => world = Some(value.to_lowercase_string()?),

                    _ => return yaml_err_unknown_key!(data.id(), key),
                }
            }
            let area = LayerTagReference::try_from_yaml(data.id().to_owned(), &(world, area))?;
            Ok((actortag, area))
        }
        _ => yaml_err!(
            data.id(),
            format!(
                "supported types for area tag reference are string, \
                 two element string list [<area>, <actortag>] \
                 or key-value table. found: {}",
                data.datatype_name()
            )
        ),
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for EnteredLeftAreaCondition {
    type Id = bool;
    // ------------------------------------------------------------------------
    fn try_from_yaml(has_entered: bool, data: &Parser) -> Result<Self> {
        // trace!(">> found entered-area-condition [{}] definition...", name);
        let (actortag, area) = extract_area_tag(data)?;

        let condition = EnteredLeftAreaCondition::new(actortag, has_entered, Some(area));

        condition.validate(data.id()).and(Ok(condition))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for InsideOutsideAreaCondition {
    type Id = bool;
    // ------------------------------------------------------------------------
    fn try_from_yaml(is_inside: bool, data: &Parser) -> Result<Self> {
        // trace!(">> found inside/outside-area-condition definition...");
        let (actortag, area) = extract_area_tag(data)?;

        let condition = InsideOutsideAreaCondition::new(actortag, is_inside, Some(area));

        condition.validate(data.id()).and(Ok(condition))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// impl TimeConditionTrait for TimeCondition {
impl<'a> TryFromYaml<Parser<'a>> for TimeCondition {
    type Id = TimeConditionType;
    // ------------------------------------------------------------------------
    fn try_from_yaml(conditiontype: TimeConditionType, data: &Parser) -> Result<Self> {
        // waituntil.beforeafternoon:
        //   before: "17:00"
        //
        // waituntil.morning:
        //   after: "05:00"
        //
        // waituntil.noon:
        //   time: "12:00"
        //
        // waituntil.delayed:
        //   elapsed: "00:01:10"
        //
        // checkif.morning:
        //   time: ["05:00", "10:00"]
        use self::TimeConditionType::*;

        let condition = match conditiontype {
            Before => {
                TimeCondition::Before(Time::try_from(data.as_str()?).map_err(|e| yaml_errmsg!(data.id(), e))?)
            }
            After => {
                TimeCondition::After(Time::try_from(data.as_str()?).map_err(|e| yaml_errmsg!(data.id(), e))?)
            }

            Range => match data.datatype() {
                YamlType::String => {
                    TimeCondition::Time(Time::try_from(data.as_str()?).map_err(|e| yaml_errmsg!(data.id(), e))?)
                }

                YamlType::Array => {
                    let list = data.to_list_of_strings()?;
                    if list.len() == 2 {
                        let from = Time::try_from(list[0].as_str())
                            .map_err(|e| yaml_errmsg!(data.id(), e))?;
                        let to = Time::try_from(list[1].as_str())
                            .map_err(|e| yaml_errmsg!(data.id(), e))?;
                        TimeCondition::TimePeriod(from, to)
                    } else {
                        return yaml_err!(data.id(), format!(
                            "expected two element string list [<from>, <to>]. found: {} elements", list.len()));
                    }
                }
                _ => {
                    return yaml_err!(
                        data.id(),
                        format!(
                            "supported types for time condition are string or
                        two element string list [<from>, <to>]. found: {}",
                            data.datatype_name()
                        )
                    )
                }
            },
            Elapsed => {
                TimeCondition::Elapsed(HiResTime::try_from(data.as_str()?).map_err(|e| yaml_errmsg!(data.id(), e))?)
            }
        };
        condition.validate(data.id()).and(Ok(condition))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for InteractionCondition {
    type Id = Option<InteractionType>;
    // ------------------------------------------------------------------------
    fn try_from_yaml(interactiontype: Option<InteractionType>, data: &Parser) -> Result<Self> {
        // waituntil.beforeafternoon:
        //   examined: prologue/someclue1        # shortcut for interaction: [examine, prologue/someclue1]
        //   talked: ~someone                    # shortcut for interaction: [talk, ~someone]
        //   talked: someone                     # shortcut for interaction: [talk, someone]
        //   used: prologue/teleport             # shortcut for interaction: [use, prologue/teleport]
        //   looted: prologue/someloot           # shortcut for interaction: [loot, prologue/someloot]
        //   interaction: [interactionname, prologue/something]      # interactionname unchecked, referenced interactive entity checked
        //   interaction: [interactionname, someone]                 # interactionname unchecked, referenced template checked
        //   interaction: [interactionname, ~componentownertag]      # completely unchecked

        let condition = match interactiontype {
            Some(ref interaction) => InteractionCondition::new(
                interaction.clone(),
                Some(InteractionReference::try_from_yaml((), data)?),
            ),
            None => {
                let list = data.as_list()?;
                let list = list.as_parser_list();

                if list.len() == 2 {
                    InteractionCondition::new(
                        InteractionType::Custom(list[0].to_string()?),
                        Some(InteractionReference::try_from_yaml((), &list[1])?),
                    )
                } else {
                    return yaml_err!(data.id(), format!(
                        "expected two element string list [<interactionname>, <ownerreference>]. found: {} elements", list.len()));
                }
            }
        };

        condition.validate(data.id()).and(Ok(condition))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LoadingScreenCondition {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let is_shown = match data.to_lowercase_string()?.as_str() {
            "shown" => true,
            "hidden" => false,
            unknown => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "expected either \"shown\" or \"hidden\". found: {}",
                        unknown
                    )
                )
            }
        };
        let condition = LoadingScreenCondition::new(is_shown);
        condition.validate(data.id()).and(Ok(condition))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
