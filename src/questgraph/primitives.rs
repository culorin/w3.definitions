//
// questgraph::primitives
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};
use yaml::parser::{Parser, YamlType};
use yaml::Yaml;
use {IntoYaml, Result, TryFromYaml};

use model::questgraph::primitives::{EditorBlockData, InSocketId, LinkTarget, OutSocketId};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LinkTarget {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let target = data.as_str()?;

        let parts = target.split('.').collect::<Vec<_>>();

        let (blocktype, blockname) = match parts.len() {
            1 => (target, ""),
            2 => (parts[0], parts[1]),
            _ => {
                return yaml_err!(format!(
                    "{}: found invalid link target (no dots in target blockname allowed): {}",
                    data.id(),
                    target
                ))
            }
        };

        let normalized_blocktype = &blocktype.to_lowercase();

        if blockname.is_empty()
            && (!normalized_blocktype.is_empty()
                && normalized_blocktype != "in"
                && normalized_blocktype != "out"
                && normalized_blocktype != "start"
                && normalized_blocktype != "end")
        {
            return yaml_err!(format!(
                "{}: missing target blockname: {}",
                data.id(),
                target
            ));
        }

        match normalized_blocktype.as_str() {
            "" => match blockname {
                "done" => Ok(LinkTarget::DeadEndMarker),
                _ => yaml_err!(format!(
                    "{}: found invalid linkend-marker: {}",
                    data.id(),
                    target
                )),
            },
            "in" => yaml_err!(format!(
                "{}: in-blocks cannot be linked to. found: {}",
                data.id(),
                target
            )),
            "start" => yaml_err!(format!(
                "{}: quest start-blocks cannot be linked to. found: {}",
                data.id(),
                target
            )),
            // shortcut for default blocknames
            "out" if blockname.is_empty() => Ok(LinkTarget::SegmentOut("Out".into())),
            "end" if blockname.is_empty() => Ok(LinkTarget::QuestEnd("Out".into())),
            "out" => Ok(LinkTarget::SegmentOut(blockname.into())),
            "end" => Ok(LinkTarget::QuestEnd(blockname.into())),

            "script" => Ok(LinkTarget::Script(blockname.into())),
            "waituntil" => Ok(LinkTarget::WaitUntil(blockname.into())),
            "subsegment" => Ok(LinkTarget::SubSegment(blockname.into())),
            "changelayers" => Ok(LinkTarget::Layers(blockname.into())),
            "journal" => Ok(LinkTarget::JournalEntry(blockname.into())),
            "objective" => Ok(LinkTarget::JournalObjective(blockname.into())),
            "mappin" => Ok(LinkTarget::JournalMappin(blockname.into())),
            "phaseobjectives" => Ok(LinkTarget::JournalPhaseObjectives(blockname.into())),
            "questoutcome" => Ok(LinkTarget::JournalQuestOutcome(blockname.into())),
            "communities" | "spawnsets" => Ok(LinkTarget::Spawnsets(blockname.into())),
            "spawn" => Ok(LinkTarget::Spawn(blockname.into())),
            "despawn" => Ok(LinkTarget::Despawn(blockname.into())),

            "scene" => Ok(LinkTarget::Scene(blockname.into())),
            "interaction" => Ok(LinkTarget::Interaction(blockname.into())),

            "pausetime" => Ok(LinkTarget::PauseTime(blockname.into())),
            "unpausetime" => Ok(LinkTarget::UnpauseTime(blockname.into())),
            "settime" => Ok(LinkTarget::SetTime(blockname.into())),
            "shifttime" => Ok(LinkTarget::ShiftTime(blockname.into())),

            "addfact" => Ok(LinkTarget::AddFact(blockname.into())),
            "reward" => Ok(LinkTarget::Reward(blockname.into())),
            "teleport" => Ok(LinkTarget::Teleport(blockname.into())),
            "changeworld" => Ok(LinkTarget::ChangeWorld(blockname.into())),
            "randomize" => Ok(LinkTarget::Randomize(blockname.into())),

            _ => yaml_err!(format!(
                "{}: found unknown blocktype: {}",
                data.id(),
                blocktype
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for EditorBlockData {
    type Id = bool;
    // ------------------------------------------------------------------------
    fn try_from_yaml(read_sockets: bool, data: &Parser) -> Result<Self> {
        let mut editor_data = EditorBlockData::default();
        // <blocktype>.<name>:
        //   .editor:
        //     pos: [1.0, 2.0]
        //     hint: "string"
        //     in: [In, In2, In3]
        //     out: [Out, Out2]
        //
        // or
        //   .editor: [1.0, 2.0]
        //
        match data.datatype() {
            YamlType::Array => {
                let values = data.to_list_of_n_floats(2)?;
                editor_data.pos = (values[0], values[1]);
            }
            _ => {
                let definition = data.as_map()?;

                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.to_lowercase().as_str() {
                        "pos" => {
                            let values = value.to_list_of_n_floats(2)?;
                            editor_data.pos = (values[0], values[1]);
                        }
                        "hint" => editor_data.hint = Some(value.to_lowercase_string()?),
                        "in" => {
                            if read_sockets {
                                editor_data.in_sockets = value
                                    .to_list_of_strings()?
                                    .iter()
                                    .map(|s| s.as_str().into())
                                    .collect::<Vec<_>>();
                            }
                        }
                        "out" => {
                            if read_sockets {
                                editor_data.out_sockets = value
                                    .to_list_of_strings()?
                                    .iter()
                                    .map(|s| s.as_str().into())
                                    .collect::<Vec<_>>();
                            }
                        }
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
        }
        Ok(editor_data)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for InSocketId {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        (&self.0).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for OutSocketId {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        (&self.0).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for EditorBlockData {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        // <blocktype>.<name>:
        //   .editor:
        //     pos: [1.0, 2.0]
        //     hint: "string"
        //     in: []
        //     out: []
        //   ...
        //   next: ...

        if self.hint.is_some() || !self.in_sockets.is_empty() || !self.out_sockets.is_empty() {
            let mut map = MapEncoder::new();

            map.add("pos", self.pos);
            map.add("hint", self.hint.as_ref());
            map.add(
                "in",
                Yaml::Array(
                    true,
                    self.in_sockets
                        .iter()
                        .map(IntoYaml::into_yaml)
                        .collect::<Vec<_>>(),
                ),
            );
            map.add(
                "out",
                Yaml::Array(
                    true,
                    self.out_sockets
                        .iter()
                        .map(IntoYaml::into_yaml)
                        .collect::<Vec<_>>(),
                ),
            );

            map.intoyaml()
        } else {
            self.pos.intoyaml()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
