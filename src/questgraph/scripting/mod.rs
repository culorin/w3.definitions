//
// questgraph::scripting - quest block scriptcalls (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use {IntoYaml, Result, TryFromYaml, ValidatableElement};
use yaml::Yaml;
use yaml::parser::Parser;
use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};

use model::questgraph::{EditorBlockData, QuestBlock};

use model::shared::{ScriptCall, ScriptParameter};
use model::questgraph::scripting::Script;

use super::common;
use super::common::*;
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Script {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found script-block [{}] definition...", name);

        let mut block = Script::new(name);
        let mut scriptcall = ScriptCall::default();

        // script.<name>:
        //   function: my_function
        //   parameter:
        //     - stringparam: "string"
        //     - floatparam: 1.0
        //     - intparam: 1
        //     - boolparam: true
        //   next: ...

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(true, &value)?);
                }
                "function" => {
                    scriptcall.set_function(value.as_str()?);
                }
                "parameter" => {
                    let paramlist = value.as_list()?;

                    for parser in paramlist.iter() {
                        let map = parser.as_map()?;
                        let (name, value) = map.as_key_value()?;

                        scriptcall.add_param(ScriptParameter::try_from_yaml(name, &value)?);
                    }
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        let scriptcall = scriptcall.validate(data.id()).and(Ok(scriptcall))?;

        block.set_function_call(scriptcall);
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for Script {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing script-block [{}]...",
            self.id().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // script.<name>:
        //   function: my_function
        //   parameter:
        //     - stringparam: "string"
        //     - floatparam: 1.0
        //     - intparam: 1
        //     - boolparam: true
        //   next: ...
        common::serialize_editordata(self, &mut map);

        if let Some(scriptcall) = self.scriptcall() {
            map.add("function", scriptcall.function());
            map.add("parameter", scriptcall.params().into_yaml());
        }

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
