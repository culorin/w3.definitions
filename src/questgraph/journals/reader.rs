//
// questgraph::journals::reader - quest block journal (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::Parser;
use {Result, TryFromYaml, ValidatableElement};

use model::questgraph::journals::{
    JournalElementReference, JournalEntry, JournalMappin, JournalObjective, JournalPhaseObjectives,
    JournalQuestOutcome,
};
use model::questgraph::{EditorBlockData, QuestBlock};

use super::common::*;
// ----------------------------------------------------------------------------
// make reference path parsing type safe depending on context
struct JournalDescriptionReference(JournalElementReference);
struct JournalObjectiveReference(JournalElementReference);
struct JournalPhaseObjectivesReference(JournalElementReference);
struct JournalQuestOutcomeReference(JournalElementReference);
struct JournalMappinReference(JournalElementReference);
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalDescriptionReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let path = data.to_lowercase_string()?;
        let parts: Vec<&str> = path.split('/').collect();

        match parts.len() {
            3 => match parts[0] {
                s if s.starts_with("characters") => Ok(JournalDescriptionReference(
                    JournalElementReference::Character(parts[1].to_owned(), parts[2].to_owned()),
                )),

                s if s.starts_with("creatures") => Ok(JournalDescriptionReference(
                    JournalElementReference::Creature(parts[1].to_owned(), parts[2].to_owned()),
                )),

                s if s.starts_with("quests") => Ok(JournalDescriptionReference(
                    JournalElementReference::Quest(parts[1].to_owned(), parts[2].to_owned()),
                )),

                unknown => yaml_err!(
                    data.id(),
                    format!(
                        "path must start with \"characters\", \"creatures\" or \
                         \"quests\". found: {}",
                        unknown
                    )
                ),
            },
            _ => yaml_err!(
                data.id(),
                format!(
                    "path must be a 3 part declaration \
                     (<\"characters\" or \"creatures\" or \"quests\">/<id>/<entryid>\"). found: {}",
                    path
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalObjectiveReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let path = data.to_lowercase_string()?;
        let parts: Vec<_> = path.split('/').collect();

        match parts.len() {
            3 => Ok(JournalObjectiveReference(
                JournalElementReference::Objective(
                    parts[0].to_owned(),
                    parts[1].to_owned(),
                    parts[2].to_owned(),
                ),
            )),
            _ => yaml_err!(
                data.id(),
                format!(
                    "path must be a 3 part declaration (<questid>/<phaseid>/<objectiveid>). \
                     found: {}",
                    &path
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalPhaseObjectivesReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let path = data.to_lowercase_string()?;
        let parts: Vec<_> = path.split('/').collect();

        match parts.len() {
            2 => Ok(JournalPhaseObjectivesReference(
                JournalElementReference::PhaseObjectives(parts[0].to_owned(), parts[1].to_owned()),
            )),
            _ => yaml_err!(
                data.id(),
                format!(
                    "path must be a 2 part declaration (<questid>/<phaseid>). \
                     found: {}",
                    &path
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalQuestOutcomeReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let path = data.to_lowercase_string()?;
        let parts: Vec<_> = path.split('/').collect();

        match parts.len() {
            1 => Ok(JournalQuestOutcomeReference(
                JournalElementReference::QuestOutcome(parts[0].to_owned()),
            )),
            _ => yaml_err!(
                data.id(),
                format!(
                    "path must be a questid declaration. \
                     found: {}",
                    &path
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalMappinReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let path = data.to_lowercase_string()?;
        let parts: Vec<_> = path.split('/').collect();

        match parts.len() {
            4 => Ok(JournalMappinReference(
                    JournalElementReference::Mappin(
                        parts[0].to_owned(), parts[1].to_owned(), parts[2].to_owned(), parts[3].to_owned())
                )),
            _ => yaml_err!(data.id(),
                    format!("path must be a 4 part declaration (<questid>/<phaseid>/<objectiveid>/<mappinid>). \
                        found: {}", &path)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalEntry {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found journal-block [{}] definition...", name);

        let mut block = JournalEntry::new(name);
        // journal.ingrid:
        //   entry: "characters/ingrid/main"
        //   activate_root: true                # default is false
        //   notify: false                      # default is true
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "entry" => {
                    block.set_reference(JournalDescriptionReference::try_from_yaml((), &value)?.0)
                }
                "notify" => block.set_notify(value.to_bool()?),
                "activate_root" => block.set_include_root(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalObjective {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found objective-block [{}] definition...", name);

        let mut block = JournalObjective::new(name);
        // objective.start_new_quest:
        //   objective: examplequEStname/PHaseA/identIfy
        //   notify: true
        //   track: false
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "objective" => {
                    block.set_reference(JournalObjectiveReference::try_from_yaml((), &value)?.0)
                }
                "notify" => block.set_notify(value.to_bool()?),
                "track" => block.set_track(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}

// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalPhaseObjectives {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found phaseobjectives-block [{}] definition...", name);

        let mut block = JournalPhaseObjectives::new(name);
        // phaseobjectives.start_new_quest:
        //   phase: examplequEStname/PHaseA
        //   notify: true
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "phase" => block
                    .set_reference(JournalPhaseObjectivesReference::try_from_yaml((), &value)?.0),
                "notify" => block.set_notify(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalQuestOutcome {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found questoutcome-block [{}] definition...", name);

        let mut block = JournalQuestOutcome::new(name);
        // questoutcome.start_new_quest:
        //   quest: examplequEStname
        //   notify: true
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "quest" => {
                    block.set_reference(JournalQuestOutcomeReference::try_from_yaml((), &value)?.0)
                }
                "notify" => block.set_notify(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalMappin {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found mappin-block [{}] definition...", name);

        let mut block = JournalMappin::new(name);
        // objective.start_new_quest:
        //   mappin: "examplequestname/phaseA/search/area1"
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "mappin" => {
                    block.set_reference(JournalMappinReference::try_from_yaml((), &value)?.0)
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
