//
// questgraph::common helpers for quest blocks
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
macro_rules! parse_links {
    ($block:ident, $key:ident, $value:ident) => {{
        let (outsocket, links) = parse_blocklinks($key, &$value)?;

        for link in links {
            if $block.id().blockid() == &link.0 {
                return Err(format!(
                    "found unsupported self-referencing link in segment: {}, block: {}",
                    $block.id().segmentid(),
                    $block.id().blockid()
                ));
            }
            $block.add_link_to(outsocket.clone(), link.0, link.1);
        }
    }};
}
// ----------------------------------------------------------------------------
macro_rules! parse_links_no_outsocket {
    ($block:ident, $key:ident, $value:ident) => {{
        let (_, links) = parse_blocklinks($key, &$value)?;

        for link in links {
            if $block.id().blockid() == &link.0 {
                return Err(format!(
                    "found unsupported self-referencing link in segment: {}, block: {}",
                    $block.id().segmentid(),
                    $block.id().blockid()
                ));
            }
            $block.add_link_to(None, link.0, link.1);
        }
    }};
}
// ----------------------------------------------------------------------------
pub(super) fn serialize_links(block: &dyn QuestBlock, map: &mut MapEncoder) {
    let mut no_links = true;
    for (socket, links) in block.links() {
        no_links = false;
        // special case for QuestStart and SegmentIn: outsocket is always " "
        if socket.is_default() || socket == &OutSocketId(" ".into()) {
            map.add("next", links.into_yaml());
        } else {
            map.add(format!("next.{}", socket), links.into_yaml());
        }
    }
    if no_links {
        map.add("next", Yaml::String(".done".into()))
    }
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn serialize_editordata(block: &dyn QuestBlock, map: &mut MapEncoder) {
    map.add(".editor", block.editordata().map(IntoYaml::into_yaml));
}
// ----------------------------------------------------------------------------
pub(super) fn map_inputsocket(
    value: &Parser,
    targetblock: LinkTarget,
    socketid: &str,
) -> Result<(LinkTarget, Option<InSocketId>)> {
    let socketid = match targetblock {
        LinkTarget::SubSegment(_)
        | LinkTarget::WaitUntil(_)
        | LinkTarget::Script(_)
        | LinkTarget::Layers(_)
        | LinkTarget::Spawnsets(_)
        | LinkTarget::Spawn(_)
        | LinkTarget::Despawn(_)
        | LinkTarget::QuestEnd(_)
        | LinkTarget::Scene(_)
        | LinkTarget::Interaction(_)
        | LinkTarget::PauseTime(_)
        | LinkTarget::UnpauseTime(_)
        | LinkTarget::ShiftTime(_)
        | LinkTarget::SetTime(_)
        | LinkTarget::AddFact(_)
        | LinkTarget::Reward(_)
        | LinkTarget::Teleport(_)
        | LinkTarget::ChangeWorld(_)
        | LinkTarget::Randomize(_)
        | LinkTarget::SegmentOut(_)
        | LinkTarget::DeadEndMarker => socketid,
        LinkTarget::JournalEntry(_) => match socketid.to_lowercase().as_str() {
            "activate" => "Activate",
            _ => {
                return Err(format!(
                    "{}: valid link target socketid for journal block is \"Activate\". \
                     found: {}",
                    value.id(),
                    socketid
                ))
            }
        },
        LinkTarget::JournalObjective(_) => match socketid.to_lowercase().as_str() {
            "activate" => "Activate",
            "deactivate" => "Deactivate",
            "success" => "Success",
            "failure" => "Failure",
            _ => {
                return Err(format!(
                    "{}: valid link target socketids for objective block are \
                     \"Activate\", \"Deactivate\", \"Success\" and \"Failure\". found: {}",
                    value.id(),
                    socketid
                ))
            }
        },
        LinkTarget::JournalPhaseObjectives(_) => match socketid.to_lowercase().as_str() {
            "deactivate" => "Deactivate",
            "success" => "Success",
            "failure" => "Failure",
            _ => {
                return Err(format!(
                    "{}: valid link target socketids for phase-objective block are \
                     \"Success\", \"Failure\" and \"Deactivate\". found: {}",
                    value.id(),
                    socketid
                ))
            }
        },
        LinkTarget::JournalMappin(_) => match socketid.to_lowercase().as_str() {
            "enable" => "Enable",
            "disable" => "Disable",
            _ => {
                return Err(format!(
                    "{}: valid link target socketids for mappin block are \
                     \"Enable\" and \"Disable\". found: {}",
                    value.id(),
                    socketid
                ))
            }
        },
        LinkTarget::JournalQuestOutcome(_) => match socketid.to_lowercase().as_str() {
            "success" => "Success",
            "failure" => "Failure",
            _ => {
                return Err(format!(
                    "{}: valid link target socketids for questoutcome block are \
                     \"Success\" and \"Failure\". found: {}",
                    value.id(),
                    socketid
                ))
            }
        },
    };

    Ok((targetblock, Some(InSocketId::from(socketid))))
}
// ----------------------------------------------------------------------------
type SocketLinks = (Option<OutSocketId>, Vec<(LinkTarget, Option<InSocketId>)>);
// ----------------------------------------------------------------------------
pub(super) fn parse_blocklinks(key: &str, value: &Parser) -> Result<SocketLinks> {
    // <block>:
    //   next: .done
    //   next: script.some_call
    //   next.End: .done
    //   next.SomeOutSocketId:
    //     - waitfor.stuffthathappened: OhNo
    //     - script.name
    //     - <blocktype>.<blockname>[: <inputname>]
    let mut links = Vec::new();

    let outsocketid = match key.to_lowercase().as_str() {
        "next" => None,
        s if s.len() > 5 && s.starts_with("next.") => Some((key.split_at(5).1).into()),
        _ => {
            return yaml_err!(format!(
                "{}: found invalid link format: {}",
                value.id(),
                key
            ))
        }
    };

    match value.datatype() {
        YamlType::String => match value.to_lowercase_string()?.as_str() {
            ".done" => {
                if let Some(ref socketid) = outsocketid {
                    return yaml_err!(
                        value.id(),
                        format!(
                            "usage of an outsocket for target \".done\" is invalid. found {}",
                            socketid
                        )
                    );
                }
            }
            _ => links.push((LinkTarget::try_from_yaml((), value)?, None)),
        },
        YamlType::Array => {
            let linklist = value.as_list()?;

            for link in linklist.iter() {
                match link.datatype() {
                    // note: default inputsocket/inputname will be set according to target blocktype
                    // on link creation
                    YamlType::String => match link.to_lowercase_string()?.as_str() {
                        ".done" => return yaml_err!(link.id(), String::from(".done target invalid in list of links")),
                        _ => links.push((LinkTarget::try_from_yaml((), &link)?, None)),
                    },
                    YamlType::Hash => {
                        let map = link.as_map()?;
                        let (link, insocketid) = map.as_parser_pair()?;

                        let link = LinkTarget::try_from_yaml((), &link)?;

                        links.push(map_inputsocket(value, link, insocketid.as_str()?)?);
                    },
                    _ => return yaml_err!(link.id(), format!(
                            "supported types for link target definition are string or (<block-target>: <input>) table. \
                            found: {}", link.datatype_name())),
                }
            }
        }
        _ => {
            return yaml_err!(
                value.id(),
                format!(
                    "supported types for link definition are string or list of strings. \
                     found: {}",
                    value.datatype_name()
                )
            )
        }
    }

    Ok((outsocketid, links))
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml};

use model::questgraph::primitives::{InSocketId, Link, LinkTarget, OutSocketId};
use model::questgraph::QuestBlock;
// ----------------------------------------------------------------------------
use IntoYaml;

use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};
use yaml::Yaml;
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for Link {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        //   next: .done
        //   next: script.some_call
        //   next.End: .done
        //   next.SomeOutSocketId:
        //     - waitfor.stuffthathappened: OhNo
        //     - script.name
        //     - <blocktype>.<blockname>[: <inputname>]
        let target_block = serialize_linktarget(self.target_block());
        let target_socket = self.target_socket();

        if target_socket.is_default() || target_socket == &InSocketId(" ".into()) {
            Yaml::String(target_block)
        } else {
            MapEncoder::from_key_value(target_block, format!("{}", target_socket)).intoyaml()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper functions
// ----------------------------------------------------------------------------
fn serialize_linktarget(target: &LinkTarget) -> String {
    use self::LinkTarget::*;
    // reduce block ids for in/outs
    match target {
        QuestEnd(ref name) if name.as_str() == "Out" => "end".into(),
        SegmentOut(ref name) if name.as_str() == "Out" => "out".into(),

        id => format!("{}", id),
    }
}
// ----------------------------------------------------------------------------
