//
// questgraph::misc::reader - misc quest block (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use model::questgraph::{EditorBlockData, QuestBlock};

use model::primitives::references::{LayerTagReference, RewardReference, SceneReference};
use model::primitives::Time;
use model::questgraph::misc::{
    AddFact, ChangeWorld, Interaction, Layers, Randomize, Reward, Scene, Spawnsets, Teleport,
    TimeManagement, TimeOperation,
};

use super::{Despawn, PauseTime, SetTime, ShiftTime, Spawn, UnpauseTime};

use super::common::*;
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Layers {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found changeLayers-block [{}] definition...", name);

        let mut block = Layers::new(name);

        // changelayers.<name>:
        //   sync: true
        //   purge: <bool>
        //   world: skellige
        //   show:
        //     - hanged_man
        //     - hanged_man2
        //   hide: hanged_man3
        //   next: waituntil.fact1

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "show"  => match value.datatype() {
                    YamlType::String => { block.add_layer_to_show(value.as_str()?); },
                    YamlType::Array => { block.set_layers_to_show(value.to_list_of_strings()?); },

                    _ => return yaml_err!(value.id(), format!(
                            "supported types for show layer definition are string or list of strings. \
                            found: {}", value.datatype_name())),
                },
                "hide" => match value.datatype() {
                    YamlType::String => { block.add_layer_to_hide(value.as_str()?); },
                    YamlType::Array => { block.set_layers_to_hide(value.to_list_of_strings()?); },

                    _ => return yaml_err!(value.id(), format!(
                            "supported types for hide layer definition are string or list of strings. \
                            found: {}", value.datatype_name())),
                },
                "sync"   => block.set_sync(value.to_bool()?),
                "world" => block.set_world(value.as_str()?),
                "purge" => block.set_purge(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn parse_spawnset_list(data: &Parser) -> Result<Vec<String>> {
    match data.datatype() {
        YamlType::String => Ok(vec![data.to_lowercase_string()?]),
        YamlType::Array => Ok(data.to_list_of_strings()?),
        _ => yaml_err!(
            data.id(),
            format!(
                "supported types for spawnset reference(s) are string or list of strings. \
                 found: {}",
                data.datatype_name()
            )
        ),
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Spawnsets {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found spawnsets-block [{}] definition...", name);

        let mut block = Spawnsets::new(name, None);
        // spawnsets.phase_x:
        //   phase: phase_x
        //   spawn: _child1.w2comm
        //   despawn:
        //     - _child2.w2comm
        //     - _child3.w2comm
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "phase" => block.set_phase(value.to_lowercase_string()?),
                "spawn" => {
                    block.set_spawnlist(parse_spawnset_list(&value)?.iter().map(String::as_str))
                }
                "despawn" => {
                    block.set_despawnlist(parse_spawnset_list(&value)?.iter().map(String::as_str))
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, Spawnsets> for Spawn {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Spawnsets> {
        trace!(">> found spawn-block [{}] definition...", name);

        let mut block = Spawnsets::new(name, Some(true));
        // spawn.community1:
        //   phase: phase_x
        //   spawnsets:
        //     - _child1.w2comm
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "phase" => block.set_phase(value.to_lowercase_string()?),
                "spawnsets" => {
                    block.set_spawnlist(parse_spawnset_list(&value)?.iter().map(String::as_str))
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, Spawnsets> for Despawn {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Spawnsets> {
        trace!(">> found despawn-block [{}] definition...", name);

        let mut block = Spawnsets::new(name, Some(false));
        // despawn.community1:
        //   spawnsets:
        //     - _child2.w2comm
        //     - _child3.w2comm
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "spawnsets" => {
                    block.set_despawnlist(parse_spawnset_list(&value)?.iter().map(String::as_str))
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Scene {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found scene-block [{}] definition...", name);

        let mut block = Scene::new(name);

        // scene.test_scene:
        //   scene: "somescene_name.yml"
        //   placement: prologue/scenepoint
        //   interruptable: true
        //   _fadein: true              # -> fade on loading
        //   next.[output]: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(true, &value)?);
                }
                "scene" => block.set_scene(SceneReference::try_from_yaml((), &value)?),
                "placement" | "location" => {
                    block.set_placement(LayerTagReference::try_from_yaml((), &value)?)
                }
                "interruptable" => block.set_interruptable(value.to_bool()?),
                "_fadein" => block.set_fadein(value.to_bool()?),
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Interaction {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found interaction-block [{}] definition...", name);

        let mut block = Interaction::new(name);

        // interaction.some_dialog:
        //   scene: "someinteraction_scene_name"
        //   placement: prologue/scenepoint
        //   interruptable: true
        //   actor: [tag1, tag2 ]
        //   next.[output]: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(true, &value)?);
                }
                "scene"         => block.set_scene(SceneReference::try_from_yaml((), &value)?),
                "placement" |
                "location"      => block.set_placement(LayerTagReference::try_from_yaml((), &value)?),
                "interruptable" => block.set_interruptable(value.to_bool()?),
                "actor"         => match value.datatype() {
                    YamlType::String => block.set_actors(&[value.to_string()?]),
                    YamlType::Array => block.set_actors(&value.to_list_of_strings()?),
                    _ => return yaml_err!(value.id(), format!(
                            "supported types for interaction actor definition are string or list of strings. \
                            found: {}", value.datatype_name())),
                },
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, TimeManagement> for PauseTime {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<TimeManagement> {
        trace!(">> found pausetime-block [{}] definition...", name);

        let mut block = TimeManagement::new(name, TimeOperation::Pause);
        // pausetime.eventname:
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, TimeManagement> for UnpauseTime {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<TimeManagement> {
        trace!(">> found unpausetime-block [{}] definition...", name);

        let mut block = TimeManagement::new(name, TimeOperation::Unpause);
        // unpausetime.eventname:
        //   next: [...]

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, TimeManagement> for SetTime {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<TimeManagement> {
        trace!(">> found settime-block [{}] definition...", name);

        // settime.eventname:
        //   time: HH:mm
        //   next: [...]

        let definition = data.as_map()?;
        let time = definition.get_value("time").ok_or(yaml_errmsg!(
            data.id(),
            "required \"time\" setting not found"
        ))??;

        let mut block =
            TimeManagement::new(name, TimeOperation::Set(Time::try_from(time.as_str()?)?));

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),
                // already parsed
                "time" => {}
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>, TimeManagement> for ShiftTime {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<TimeManagement> {
        trace!(">> found shifttime-block [{}] definition...", name);

        // shifttime.eventname:
        //   by: HH:mm
        //   next: [...]

        let definition = data.as_map()?;
        let time = definition
            .get_value("by")
            .ok_or(yaml_errmsg!(data.id(), "required \"by\" setting not found"))??;

        let mut block =
            TimeManagement::new(name, TimeOperation::Shift(Time::try_from(time.as_str()?)?));

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),
                // already parsed
                "by" => {}
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for AddFact {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found addfact-block [{}] definition...", name);

        let mut block = AddFact::new(name);

        // addfact.tojournal:
        //   value: ["jb1", 1]
        //   next: [...]
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "value" => {
                    let list = value.as_list()?;
                    let list = list.as_parser_list();

                    match list.len() {
                        2 => block.set_fact(list[0].as_str()?, list[1].to_integer()?),
                        _ => {
                            return yaml_err!(
                                data.id(),
                                format!(
                                    "expected two element string list [<factid>, <value>]. \
                                     found: {} elements",
                                    list.len()
                                )
                            )
                        }
                    }
                }
                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Reward {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found reward-block [{}] definition...", name);

        let mut block = Reward::new(name);

        // reward.for_player:
        //   reward: somereward_from_reward_definition
        //   next: ...
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "reward" => block.set_reward(RewardReference::try_from_yaml((), &value)?),

                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Teleport {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found teleport-block [{}] definition...", name);

        let mut block = Teleport::new(name);

        // teleport.to_somewhere:
        //   destination: prologue/waypointtag
        //   _actor: sometag
        //   next: ...
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "destination" | "target" => {
                    block.set_destination(LayerTagReference::try_from_yaml((), &value)?)
                }
                "_actor" => block.set_actor(value.as_str()?),

                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ChangeWorld {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found changeworld-block [{}] definition...", name);

        let mut block = ChangeWorld::new(name);

        // changeworld.to_somewhere:
        //   destination: prologue/waypointtag
        //   next: ...
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "destination" | "target" => {
                    block.set_destination(LayerTagReference::try_from_yaml((), &value)?)
                }

                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Randomize {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found randomize-block [{}] definition...", name);

        let mut block = Randomize::new(name);

        // randomize.to_somewhere:
        //   next: ...
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }

                link if link.starts_with("next") => parse_links!(block, key, value),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
