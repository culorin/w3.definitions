//
// questgraph::misc::writer - misc quest block (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::encoder::{IntoYaml as EncoderIntoYaml, ListEncoder, MapEncoder};
use yaml::Yaml;
use IntoYaml;

use model::questgraph::QuestBlock;

use model::questgraph::misc::{
    AddFact, ChangeWorld, Despawnset, Interaction, Layers, Randomize, Reward, Scene, Spawnset,
    Spawnsets, Teleport, TimeManagement, TimeOperation,
};
use model::questgraph::primitives::BlockId;

use super::common;
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for Layers {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing changeLayers-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // changelayers.<name>:
        //   sync: true
        //   purge: <bool>
        //   world: skellige
        //   show:
        //     - hanged_man
        //     - hanged_man2
        //   hide: hanged_man3
        //   next: waituntil.fact1
        common::serialize_editordata(self, &mut map);

        map.add("sync", self.sync().map(|b| b.intoyaml()));
        map.add("purge", self.purge().map(|b| b.intoyaml()));
        map.add("world", self.world());
        map.add("show", self.show().into_yaml());
        map.add("hide", self.hide().into_yaml());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Spawnsets {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing spawnsets-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // spawnsets.phase_x:
        //   phase: phase_x
        //   spawn: _child1.w2comm
        //   despawn:
        //     - _child2.w2comm
        //     - _child3.w2comm
        //   next: [...]
        //
        // -- special case for spawn/despawn blocks
        //
        // spawn.community1:
        //   phase: phase_x
        //   spawnsets:
        //     - _child1.w2comm
        //   next: [...]
        //
        // despawn.community1:
        //   spawnsets:
        //     - _child2.w2comm
        //     - _child3.w2comm
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        match self.uid().blockid() {
            BlockId::Spawn(_) => {
                map.add("phase", self.phase());
                map.add("spawnsets", self.spawn().into_yaml());
            }
            BlockId::Despawn(_) => {
                map.add("spawnsets", self.despawn().into_yaml());
            }
            _ => {
                map.add("phase", self.phase());
                map.add("spawn", self.spawn().into_yaml());
                map.add("despawn", self.despawn().into_yaml());
            }
        }

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Spawnset {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        self.id().intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Despawnset {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        self.id().intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Scene {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing scene-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // scene.test_scene:
        //   scene: "somescene_name.yml"
        //   placement: prologue/scenepoint
        //   interruptable: true
        //   _fadein: true
        //   next.[output]: [...]
        common::serialize_editordata(self, &mut map);

        map.add("scene", self.scene().into_yaml());
        map.add("placement", self.placement().into_yaml());
        map.add("interruptable", *self.interruptable());
        map.add("_fadein", *self.fadein());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Interaction {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing interaction-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // interaction.some_dialog:
        //   scene: "someinteraction_scene_name"
        //   placement: prologue/scenepoint
        //   interruptable: true
        //   actor: [tag1, tag2 ]
        //   next.[output]: [...]
        common::serialize_editordata(self, &mut map);

        map.add("scene", self.scene().into_yaml());
        map.add("placement", self.placement().into_yaml());
        map.add("interruptable", *self.interruptable());
        map.add("actor", ListEncoder::from_iter_inlined(self.actor()));

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for TimeManagement {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::TimeOperation::*;

        trace!(
            ">> serializing timemanagement-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // pausetime.eventname:
        //   next: [...]
        // unpausetime.eventname:
        //   next: [...]
        // settime.eventname:
        //   time: HH:mm
        //   next: [...]
        // shifttime.eventname:
        //   by: HH:mm
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        match self.operation() {
            Pause | Unpause => {}
            Set(time) => map.add("time", time.into_yaml()),
            Shift(time) => map.add("by", time.into_yaml()),
        }

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for AddFact {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing addfact-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // addfact.tojournal:
        //   value: ["jb1", 1]
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        let mut data = ListEncoder::new_inlined();
        data.add(self.fact());
        data.add(self.value().map(|v| v.intoyaml()));

        map.add("value", data);

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Reward {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing reward-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // reward.for_player:
        //   reward: somereward_from_reward_definition
        //   next: ...
        common::serialize_editordata(self, &mut map);

        map.add("reward", self.reward().into_yaml());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Teleport {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing teleport-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // teleport.to_somewhere:
        //   destination: prologue/waypointtag
        //   _actor: sometag
        //   next: ...
        common::serialize_editordata(self, &mut map);

        map.add("destination", self.destination().into_yaml());
        map.add("_actor", self.actor());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ChangeWorld {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing changeworld-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // changeworld.to_somewhere:
        //   destination: prologue/waypointtag
        //   next: ...
        common::serialize_editordata(self, &mut map);

        map.add("destination", self.destination().into_yaml());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Randomize {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing randomize-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // randomize.to_somewhere:
        //   destination: prologue/waypointtag
        //   next: ...
        common::serialize_editordata(self, &mut map);
        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
