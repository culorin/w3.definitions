//
// questgraph::misc - misc quest block (yaml) definitions
//
// ----------------------------------------------------------------------------
// make community block parsing type safe depending on context
pub(super) struct Spawn(Spawnsets);
pub(super) struct Despawn(Spawnsets);
// ----------------------------------------------------------------------------
// make timemanagement block parsing type safe depending on context
pub(super) struct PauseTime(TimeManagement);
pub(super) struct UnpauseTime(TimeManagement);
pub(super) struct ShiftTime(TimeManagement);
pub(super) struct SetTime(TimeManagement);
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
use super::common;
use model::questgraph::misc::{Spawnsets, TimeManagement};
// ----------------------------------------------------------------------------
