//
// questgraph::segments - structure quest blocks (yaml) definitions
//
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
use IntoYaml;

use model::questgraph::SegmentBlock;
use yaml::Yaml;

use super::common;
use super::misc;
// ----------------------------------------------------------------------------
impl IntoYaml for SegmentBlock {
    type Output = Yaml;
    fn into_yaml(&self) -> Self::Output {
        match *self {
            SegmentBlock::SubSegment(ref o) => o.into_yaml(),
            SegmentBlock::WaitUntil(ref o) => o.into_yaml(),
            SegmentBlock::Script(ref o) => o.into_yaml(),
            SegmentBlock::Layers(ref o) => o.into_yaml(),
            SegmentBlock::Spawnsets(ref o) => o.into_yaml(),
            SegmentBlock::JournalEntry(ref o) => o.into_yaml(),
            SegmentBlock::JournalObjective(ref o) => o.into_yaml(),
            SegmentBlock::JournalMappin(ref o) => o.into_yaml(),
            SegmentBlock::JournalPhaseObjectives(ref o) => o.into_yaml(),
            SegmentBlock::JournalQuestOutcome(ref o) => o.into_yaml(),
            SegmentBlock::Interaction(ref o) => o.into_yaml(),
            SegmentBlock::Scene(ref o) => o.into_yaml(),
            SegmentBlock::TimeManagement(ref o) => o.into_yaml(),
            SegmentBlock::AddFact(ref o) => o.into_yaml(),
            SegmentBlock::Reward(ref o) => o.into_yaml(),
            SegmentBlock::Teleport(ref o) => o.into_yaml(),
            SegmentBlock::ChangeWorld(ref o) => o.into_yaml(),
            SegmentBlock::Randomize(ref o) => o.into_yaml(),
        }
    }
}
// ----------------------------------------------------------------------------
