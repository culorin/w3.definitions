//
// questgraph::segments::writer - structure quest blocks (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use IntoYaml;

use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};
use yaml::Yaml;

use model::questgraph::primitives::{BlockId, BlockUid};
use model::questgraph::segments::{
    QuestEnd, QuestRootSegment, QuestSegment, QuestStart, SegmentIn, SegmentOut, SubSegment,
};
use model::questgraph::QuestBlock;

use super::common;
// ----------------------------------------------------------------------------
// TryIntoYaml impl for different quest segments (aka phases)
// ----------------------------------------------------------------------------
impl IntoYaml for QuestRootSegment {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        debug!("> serializing root segment...");

        // blocks:
        //   start: ...
        //   end: ...
        //   subsegment: ...
        let mut blocks = MapEncoder::new();
        for block in self.start_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }
        for block in self.segment_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }
        for block in self.end_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }

        MapEncoder::from_key_value("blocks", blocks).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for QuestSegment {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        debug!("> serializing subsegment [{}]...", self.id());

        let mut map = MapEncoder::new();
        // segmentname:
        //   external: true/false
        //   in: [...]
        //   out: [...]
        //   blocks: {...}

        if *self.external() {
            map.add("external", true);
        }

        let mut blocks = MapEncoder::new();
        for block in self.in_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }
        for block in self.segment_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }
        for block in self.out_blocks() {
            blocks.add(serialize_blockid(block.uid()), block.into_yaml());
            blocks.add_newline();
        }

        map.add("blocks", blocks);
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl for different blocks
// ----------------------------------------------------------------------------
impl IntoYaml for QuestStart {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(">> serializing quest start-block...");

        let mut map = MapEncoder::new();
        // start:
        //   next:              # only valid outsocketid is " "
        //     - end.id1: in2
        //     - end.id2
        common::serialize_editordata(self, &mut map);

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for QuestEnd {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing quest end-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // end.success: .done
        // end.failed: .done
        common::serialize_editordata(self, &mut map);
        map.add("next", ".done");

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for SegmentIn {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing segment in-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // in.name:
        //   next:              # only valid outsocketid is " "
        //     - out.failed
        //     - script.my_func
        common::serialize_editordata(self, &mut map);
        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for SegmentOut {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing segment out-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // out.success: .done
        // out.failed: .done
        common::serialize_editordata(self, &mut map);
        map.add("next", ".done");

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for SubSegment {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    #[allow(clippy::redundant_closure)]
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing subsegment-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // subsegment.a_subsegment:     # no socketid definition: inputname is directly piped into graph
        //   world: "some world id"     # only valid in top level questsegment?
        //   segment: test_seg
        //   next.success:
        //     - end
        //   next.failure:
        //     - end.out2
        common::serialize_editordata(self, &mut map);

        map.add("world", self.required_world());
        map.add("segment", self.segmentlink().map(|l| l.into_yaml()));

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper functions
// ----------------------------------------------------------------------------
fn serialize_blockid(uid: &BlockUid) -> String {
    use self::BlockId::*;
    // reduce block ids for in/outs
    match uid.blockid() {
        QuestStart(_) => "start".into(),
        QuestEnd(ref name) if name.as_str() == "Out" => "end".into(),

        SegmentIn(ref name) if name.as_str() == "In" => "in".into(),
        SegmentOut(ref name) if name.as_str() == "Out" => "out".into(),

        id => format!("{}", id),
    }
}
// ----------------------------------------------------------------------------
