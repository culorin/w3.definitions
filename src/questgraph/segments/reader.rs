//
// questgraph::segments::reader - structure quest blocks (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use model::questgraph::conditions::WaitUntil;
use model::questgraph::journals::{
    JournalEntry, JournalMappin, JournalObjective, JournalPhaseObjectives, JournalQuestOutcome,
};
use model::questgraph::misc::{
    AddFact, ChangeWorld, Interaction, Layers, Randomize, Reward, Scene, Spawnsets, Teleport,
};
use model::questgraph::scripting::Script;
use model::questgraph::segments::{
    QuestEnd, QuestRootSegment, QuestSegment, QuestStart, SegmentIn, SegmentOut, SubSegment,
};
use model::questgraph::{EditorBlockData, QuestBlock, SegmentId};

use super::common::*;
use super::misc::{Despawn, PauseTime, SetTime, ShiftTime, Spawn, UnpauseTime};
// ----------------------------------------------------------------------------
// TryFromYaml impl for different quest segments (aka phases)
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestRootSegment {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let mut quest = QuestRootSegment::default();

        // blocks:
        //   start: ...
        //   end: ...
        //   subsegment: ...

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            let (blocktype, name) = parse_blockname(data.id(), key)?;

            match blocktype.to_lowercase().as_str() {
                // --- special blocks valid only in root segment
                "start" | "queststart" => quest.set_start(QuestStart::try_from_yaml(name, &value)?),
                "end" | "questend" => quest.add_end(QuestEnd::try_from_yaml(name, &value)?),
                // --- blocks valid in all segments
                "waituntil" => quest.add_block(WaitUntil::try_from_yaml(name, &value)?),
                "script" => quest.add_block(Script::try_from_yaml(name, &value)?),
                "subsegment" => quest.add_block(SubSegment::try_from_yaml(name, &value)?),
                "changelayers" => quest.add_block(Layers::try_from_yaml(name, &value)?),
                "journal" => quest.add_block(JournalEntry::try_from_yaml(name, &value)?),
                "objective" => quest.add_block(JournalObjective::try_from_yaml(name, &value)?),
                "mappin" => quest.add_block(JournalMappin::try_from_yaml(name, &value)?),
                "phaseobjectives" => {
                    quest.add_block(JournalPhaseObjectives::try_from_yaml(name, &value)?)
                }
                "questoutcome" => {
                    quest.add_block(JournalQuestOutcome::try_from_yaml(name, &value)?)
                }
                "communities" | "spawnsets" => {
                    quest.add_block(Spawnsets::try_from_yaml(name, &value)?)
                }
                "spawn" => quest.add_block(Spawn::try_from_yaml(name, &value)?),
                "despawn" => quest.add_block(Despawn::try_from_yaml(name, &value)?),

                "scene" => quest.add_block(Scene::try_from_yaml(name, &value)?),
                "interaction" => quest.add_block(Interaction::try_from_yaml(name, &value)?),

                "pausetime" => quest.add_block(PauseTime::try_from_yaml(name, &value)?),
                "unpausetime" => quest.add_block(UnpauseTime::try_from_yaml(name, &value)?),
                "settime" => quest.add_block(SetTime::try_from_yaml(name, &value)?),
                "shifttime" => quest.add_block(ShiftTime::try_from_yaml(name, &value)?),

                "addfact" => quest.add_block(AddFact::try_from_yaml(name, &value)?),
                "reward" => quest.add_block(Reward::try_from_yaml(name, &value)?),
                "teleport" => quest.add_block(Teleport::try_from_yaml(name, &value)?),
                "changeworld" => quest.add_block(ChangeWorld::try_from_yaml(name, &value)?),
                "randomize" => quest.add_block(Randomize::try_from_yaml(name, &value)?),

                unknown => yaml_err!(format!(
                    "{}: unsupported block type found: {}",
                    value.id(),
                    unknown
                )),
            }?
        }

        quest.validate(data.id()).and(Ok(quest))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestSegment {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        debug!("> found subsegment [{}] definition...", &name);

        let mut segment = QuestSegment::new(name);

        // segmentname:
        //   external: true/false
        //   in: [...]
        //   out: [...]
        //   blocks: {...}

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "external" => segment.set_external(value.to_bool()?),
                "in" => warn!("TODO implement input definition parsing on subsegments"),
                "out" => warn!("TODO implement output definition parsing on subsegments"),
                "blocks" => {
                    let block_definition = value.as_map()?;

                    for kvparser in block_definition.iter() {
                        let (key, value) = kvparser.to_key_value()?;
                        let (blocktype, name) = parse_blockname(data.id(), key)?;

                        match blocktype.to_lowercase().as_str() {
                            // --- special blocks valid only in (sub)segments
                            "in" => segment.add_input(SegmentIn::try_from_yaml(name, &value)?),
                            "out" => segment.add_output(SegmentOut::try_from_yaml(name, &value)?),
                            // --- blocks valid in all segments
                            "waituntil" => {
                                segment.add_block(WaitUntil::try_from_yaml(name, &value)?)
                            }
                            "script" => segment.add_block(Script::try_from_yaml(name, &value)?),
                            "subsegment" => {
                                segment.add_block(SubSegment::try_from_yaml(name, &value)?)
                            }
                            "changelayers" => {
                                segment.add_block(Layers::try_from_yaml(name, &value)?)
                            }
                            "journal" => {
                                segment.add_block(JournalEntry::try_from_yaml(name, &value)?)
                            }
                            "objective" => {
                                segment.add_block(JournalObjective::try_from_yaml(name, &value)?)
                            }
                            "mappin" => {
                                segment.add_block(JournalMappin::try_from_yaml(name, &value)?)
                            }
                            "phaseobjectives" => segment
                                .add_block(JournalPhaseObjectives::try_from_yaml(name, &value)?),
                            "questoutcome" => {
                                segment.add_block(JournalQuestOutcome::try_from_yaml(name, &value)?)
                            }
                            "communities" | "spawnsets" => {
                                segment.add_block(Spawnsets::try_from_yaml(name, &value)?)
                            }
                            "spawn" => segment.add_block(Spawn::try_from_yaml(name, &value)?),
                            "despawn" => segment.add_block(Despawn::try_from_yaml(name, &value)?),

                            "scene" => segment.add_block(Scene::try_from_yaml(name, &value)?),
                            "interaction" => {
                                segment.add_block(Interaction::try_from_yaml(name, &value)?)
                            }

                            "pausetime" => {
                                segment.add_block(PauseTime::try_from_yaml(name, &value)?)
                            }
                            "unpausetime" => {
                                segment.add_block(UnpauseTime::try_from_yaml(name, &value)?)
                            }
                            "settime" => segment.add_block(SetTime::try_from_yaml(name, &value)?),
                            "shifttime" => {
                                segment.add_block(ShiftTime::try_from_yaml(name, &value)?)
                            }

                            "addfact" => segment.add_block(AddFact::try_from_yaml(name, &value)?),
                            "reward" => segment.add_block(Reward::try_from_yaml(name, &value)?),
                            "teleport" => segment.add_block(Teleport::try_from_yaml(name, &value)?),
                            "changeworld" => {
                                segment.add_block(ChangeWorld::try_from_yaml(name, &value)?)
                            }
                            "randomize" => {
                                segment.add_block(Randomize::try_from_yaml(name, &value)?)
                            }

                            unknown => yaml_err!(format!(
                                "{}: unsupported block type found: {}",
                                value.id(),
                                unknown
                            )),
                        }?
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        segment.validate(data.id()).and(Ok(segment))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryFromYaml impl for different blocks
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestStart {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found quest start-block [{}] definition...", name);

        let mut block = QuestStart::new(name);

        // start:               # quest have only a single insocketid "start.in"
        //   next:              # only valid outsocketid is " "
        //     - end.id1: in2
        //     - end.id2

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "next" => parse_links_no_outsocket!(block, key, value),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestEnd {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found quest end-block [{}] definition...", name);

        let mut block = QuestEnd::new(name);

        // end.success: .done
        // end.failed:
        //   next: .done
        match data.datatype() {
            // legacy definition
            YamlType::String => match data.to_lowercase_string()?.as_str() {
                ".done" => {}
                unknown => {
                    return yaml_err!(format!(
                        "{}: only \".done\" marker allowed for quest-end block. found: {} ",
                        data.id(),
                        unknown
                    ))
                }
            },
            _ => {
                let definition = data.as_map()?;

                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.to_lowercase().as_str() {
                        ".editor" => {
                            block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                        }
                        "next" => {
                            if value.to_lowercase_string()? != ".done" {
                                return yaml_err!(format!(
                                "{}: only \".done\" link supported in quest-end blocks. found: {} ",
                                data.id(),
                                value.as_str()?
                            ));
                            }
                        }
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
        }

        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SegmentIn {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found segment in-block [{}] definition...", name);

        let mut block = SegmentIn::new(name);

        // in.name:
        //   next:              # only valid outsocketid is " "
        //     - out.failed
        //     - script.my_func

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "next" => parse_links_no_outsocket!(block, key, value),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SegmentOut {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found segment out-block [{}] definition...", name);

        let mut block = SegmentOut::new(name);

        // out.success: .done
        // out.failed:
        //   next: .done
        match data.datatype() {
            // legacy definition
            YamlType::String => match data.to_lowercase_string()?.as_str() {
                ".done" => {}
                unknown => {
                    return yaml_err!(format!(
                        "{}: only \".done\" marker allowed for segment-out block. found: {} ",
                        data.id(),
                        unknown
                    ))
                }
            },
            _ => {
                let definition = data.as_map()?;

                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.to_lowercase().as_str() {
                        ".editor" => {
                            block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                        }
                        "next" => {
                            if value.to_lowercase_string()? != ".done" {
                                return yaml_err!(format!(
                                "{}: only \".done\" link supported in segment-out blocks. found: {} ",
                                data.id(),
                                value.as_str()?
                            ));
                            }
                        }
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SubSegment {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found subsegment-block [{}] definition...", name);

        let mut block = SubSegment::new(name);

        // subsegment.a_subsegment:     # no socketid definition: inputname is directly piped into graph
        //   world: "some world id"     # only valid in top level questsegment?
        //   segment: test_seg
        //   next.success:
        //     - end
        //   next.failure:
        //     - end.out2

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".editor" => {
                    block.set_editordata(EditorBlockData::try_from_yaml(false, &value)?);
                }
                "segment" => {
                    block.set_segmentlink(SegmentId::from(value.as_str()?));
                }
                "world" => {
                    block.set_required_world(value.as_str()?);
                }
                link if link.starts_with("next") => parse_links!(block, key, value),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        block.validate(data.id()).and(Ok(block))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper functions
// ----------------------------------------------------------------------------
fn parse_blockname<'a>(elementid: &str, blockname: &'a str) -> Result<(&'a str, &'a str)> {
    // "in" -> ("in".lowercase, "In"), etc.
    match blockname.find('.') {
        Some(splitpoint) => match splitpoint {
            0 => yaml_err!(format!(
                "{}: block type missing in block key: {}",
                elementid, blockname
            )),
            _ => Ok((
                blockname.split_at(splitpoint).0,
                blockname.split_at(splitpoint + 1).1,
            )),
        },
        None => match blockname.to_lowercase().as_str() {
            "in" => Ok(("in", "In")),
            "out" => Ok(("out", "Out")),
            "start" => Ok(("start", "In")),
            "end" => Ok(("end", "Out")),
            unknown => yaml_err!(format!(
                "{}: unsupported block type found: {}",
                elementid, unknown
            )),
        },
    }
}
// ----------------------------------------------------------------------------
