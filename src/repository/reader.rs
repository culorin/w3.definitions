// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::path::Path;

use model::rtti::CustomRttiDefinition;

use super::EntityMeshRegistry;
use super::{ActionInfo, FoliageInfo, JournalGroupInfo, MeshComponentInfo, WorldInfo};
use super::{DatasetContainer, StatsOutput};
use super::{JournalGroup, Repository};
use super::{MapParser, Parser, Result, TryFromYaml, ValidatableElement, Yaml, YamlType};
use super::{Rotation, Vector3D};
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct RepositoryDataset {
    worlds: Vec<(String, WorldInfo)>,
    actions: Vec<(String, ActionInfo)>,
    foliage: Vec<(String, FoliageInfo)>,
    entity_meshes: EntityMeshRegistry,
    journalgroups: Vec<JournalGroup>,
    rtti: CustomRttiDefinition,
}
// ----------------------------------------------------------------------------
impl RepositoryDataset {
    // ------------------------------------------------------------------------
    fn parse_repository(&mut self, datasrc: &str, data: &Parser) -> Result<()> {
        info!("> found repository definition in {}...", datasrc);

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "worlds" => self.worlds = Self::parse_worlds(&value)?,
                "actions" => self.actions = Self::parse_actions(&value)?,
                "foliage" => self.foliage = Self::parse_foliage(&value)?,
                "journalgroups" => self.journalgroups = Self::parse_journal_groups(&value)?,
                "entity-meshes" => self.entity_meshes = Self::parse_entity_meshinfo(&value)?,

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn parse_rtti_definition(&self, datasrc: &str, data: &Parser) -> Result<CustomRttiDefinition> {
        info!("> found custom rtti repo info in {}...", datasrc);

        CustomRttiDefinition::try_from_yaml(datasrc, data)
    }
    // ------------------------------------------------------------------------
    fn parse_worlds(data: &Parser) -> Result<Vec<(String, WorldInfo)>> {
        trace!("> found world repo info...");
        let definition = data.as_map()?;

        let mut worlds = Vec::new();
        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            worlds.push((id.into(), WorldInfo::try_from_yaml((), &value)?))
        }
        Ok(worlds)
    }
    // ------------------------------------------------------------------------
    fn parse_actions(data: &Parser) -> Result<Vec<(String, ActionInfo)>> {
        trace!("> found actions repo info...");
        let definition = data.as_map()?;

        let mut actionlist = Vec::new();
        for kvparser in definition.iter() {
            let (category, value) = kvparser.to_key_value()?;

            let actions = value.as_map()?;

            for kvparser in actions.iter() {
                let (id, value) = kvparser.to_key_value()?;

                let mut action = ActionInfo::try_from_yaml(id, &value)?;
                action.set_category(category);

                actionlist.push((action.uid(), action));
            }
        }
        Ok(actionlist)
    }
    // ------------------------------------------------------------------------
    fn parse_foliage(data: &Parser) -> Result<Vec<(String, FoliageInfo)>> {
        trace!("> found foliage repo info...");
        let definition = data.as_map()?;

        let mut foliage = Vec::new();
        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            foliage.push((id.to_string(), FoliageInfo::try_from_yaml(id, &value)?));
        }
        Ok(foliage)
    }
    // ------------------------------------------------------------------------
    fn parse_entity_meshinfo(data: &Parser) -> Result<EntityMeshRegistry> {
        trace!("> found entity-meshes repo info...");
        let definition = data.as_map()?;

        let mut registry = EntityMeshRegistry::default();
        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let mut meshcomponents = Vec::new();

            for meshcomp_data in value.as_list()?.iter() {
                meshcomponents.push(MeshComponentInfo::try_from_yaml(id, &meshcomp_data)?);
            }

            registry.add(id, meshcomponents)?;
        }
        Ok(registry)
    }
    // ------------------------------------------------------------------------
    fn parse_journal_groups(data: &Parser) -> Result<Vec<JournalGroup>> {
        let definition = data.as_map()?;

        let mut journalgroups = Vec::new();
        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "creatures" => {
                    journalgroups.append(&mut Self::parse_creature_journalgroups(&value)?)
                }
                "characters" => {
                    journalgroups.append(&mut Self::parse_character_journalgroups(&value)?)
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(journalgroups)
    }
    // ------------------------------------------------------------------------
    fn parse_creature_journalgroups(data: &Parser) -> Result<Vec<JournalGroup>> {
        trace!("> found creature journal group repo info...");
        let definition = data.as_map()?;

        let mut journalgroups = Vec::new();
        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let info = JournalGroup::Creature(JournalGroupInfo::try_from_yaml(id, &value)?);

            info.validate(data.id())?;

            journalgroups.push(info);
        }
        Ok(journalgroups)
    }
    // ------------------------------------------------------------------------
    fn parse_character_journalgroups(data: &Parser) -> Result<Vec<JournalGroup>> {
        trace!("> found character journal group repo info...");
        let definition = data.as_map()?;

        let mut journalgroups = Vec::new();
        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let info = JournalGroup::Character(JournalGroupInfo::try_from_yaml(id, &value)?);

            info.validate(data.id())?;

            journalgroups.push(info);
        }
        Ok(journalgroups)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetContainer<()> for Repository {
    type Dataset = RepositoryDataset;
    // ------------------------------------------------------------------------
    fn parse(_basedir: &Path, filepath: &Path, data: &Yaml, _: &()) -> Result<Self::Dataset> {
        debug!("parsing repository definition content...");

        let datasrc = filepath.to_string_lossy();
        let data = MapParser::new("repo definition:", data)?;

        let mut dataset = RepositoryDataset::default();

        for kvparser in data.iter() {
            let (key, value) = kvparser.to_key_value()?;
            let key = key.to_lowercase();
            let key = key.as_str();

            match key {
                "repository" => dataset.parse_repository(&datasrc, &value)?,
                "rtti" => dataset.rtti = dataset.parse_rtti_definition(&datasrc, &value)?,
                _ => return yaml_err_unknown_key!("repository definition", key),
            }
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
    fn add(&mut self, mut data: Self::Dataset) -> Result<()> {
        // merge into dataset into repository
        for (id, worldinfo) in data.worlds {
            self.worlds.insert(id, worldinfo);
        }

        for (id, actioninfo) in data.actions {
            self.actions.insert(id, actioninfo);
        }

        for (id, foliageinfo) in data.foliage {
            self.foliage.insert(id, foliageinfo);
        }

        for (id, meshinfo) in data.entity_meshes.0 {
            self.entity_meshes.0.insert(id, meshinfo);
        }

        if !data.journalgroups.is_empty() {
            self.journalgroups.append(&mut data.journalgroups);
        }

        for (classname, rtti) in data.rtti.classes_mut().drain(..) {
            self.rtti.add_class(&classname, rtti)?;
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[rustfmt::skip]
impl StatsOutput for Repository {
    // ------------------------------------------------------------------------
    fn log_stats(&self) {
        info!("world meta in repository:       {}", self.worlds.len());
        info!("action meta in repository:      {}", self.actions.len());
        info!("journalgroups in repository:    {}", self.journalgroups.len());
        info!("entity-mesh info in repository: {}", self.entity_meshes.len());
        info!("foliage info in repository:     {}", self.foliage.len());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryFromYaml
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WorldInfo {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let mut info = WorldInfo::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "id" => info.world_id = value.to_integer()? as u8,
                "world" => info.world_path = value.to_lowercase_string()?.replace('/', "\\"),
                "level" => info.level_path = value.to_lowercase_string()?.replace('/', "\\"),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        info.validate(data.id()).and(Ok(info))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ActionInfo {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let mut info = ActionInfo {
            action_id: name.to_string(),
            ..Default::default()
        };

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "file" => info.file = value.to_lowercase_string()?.replace('/', "\\"),
                "place" => info.place = value.to_lowercase_string()?,
                "ignorecollisions" => info.ignore_collisions = Some(value.to_bool()?),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        info.validate(data.id()).and(Ok(info))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for FoliageInfo {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        let mut info = FoliageInfo::new(name);

        match data.datatype() {
            YamlType::String => {
                info.set_file(data.as_str()?);
            }
            _ => {
                for kvparser in data.as_map()?.iter() {
                    let (key, value) = kvparser.to_key_value()?;
                    match key.to_lowercase().as_str() {
                        "file" => info.set_file(value.as_str()?),
                        "collision" => info.set_collision(value.to_bool()?),
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                }
            }
        }
        info.validate(data.id()).and(Ok(info))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for JournalGroupInfo {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        // characters:
        //   main:
        //      file: gameplay\journal\characters\maincharacters.journal
        //      guid: 7c39d9e7-c331-447b-b409-b4585da7c909

        let mut info = JournalGroupInfo::new(name);

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "file" => info.path = value.as_str()?.replace('/', "\\"),
                "guid" => info.set_guid(value.as_str()?)?,

                _ => return yaml_err_unknown_key!(value.id(), key),
            }
        }

        info.validate(data.id()).and(Ok(info))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for MeshComponentInfo {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(_name: &str, data: &Parser) -> Result<Self> {
        // "somemeshpath":
        //   - mesh: "dlc/bob/data/environment/architecture/streets_elements/rich/bob_wall_deco_02.w2mesh"
        //     pos: [ -0.3, 0.04, 0.1 ]
        //     rot: [ 0.0, 0.0, 180.0 ]
        //     scale: [ 1.25, 1.25, 1.25 ]
        //     # .type: CMeshComponent
        //     # .name: "comp"
        //     # casts_shadows: false
        //     # is_streamed: false
        //     ignore: true

        let mut info = MeshComponentInfo::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "mesh" => info.mesh = value.as_str()?.replace('\\', "/"),
                "pos" => info.pos = Vector3D::try_from_yaml((), &value)?,
                "rot" => info.rot = Rotation::try_from_yaml((), &value)?,
                "scale" => info.scale = Vector3D::try_from_yaml((), &value)?,
                "ignore" => info.ignore = value.to_bool()?,

                _ => return yaml_err_unknown_key!(value.id(), key),
            }
        }

        info.validate(data.id()).and(Ok(info))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
