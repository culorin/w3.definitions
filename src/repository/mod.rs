//
// definitions::repository - repository structure (yaml) definitions
//
extern crate uuid;

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct Repository {
    worlds: HashMap<String, WorldInfo>,
    actions: HashMap<String, ActionInfo>,
    foliage: HashMap<String, FoliageInfo>,
    entity_meshes: EntityMeshRegistry,
    journalgroups: Vec<JournalGroup>,
    rtti: CustomRttiDefinition,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub struct WorldInfo {
    world_id: u8,
    world_path: String,
    level_path: String,
    navdata_path: String,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub struct ActionInfo {
    action_id: String,
    category: String,
    file: String,
    place: String,
    ignore_collisions: Option<bool>,
}
// ----------------------------------------------------------------------------
pub struct FoliageInfo {
    file: String,
    has_collision: Option<bool>,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct MeshComponentInfo {
    mesh: String,
    pos: Vector3D,
    rot: Rotation,
    scale: Vector3D,
    ignore: bool,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub struct JournalGroupInfo {
    name: String,
    guid: GUID,
    path: String,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum JournalGroup {
    Character(JournalGroupInfo),
    Creature(JournalGroupInfo),
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct EntityMeshRegistry(BTreeMap<String, Vec<MeshComponentInfo>>);
// ----------------------------------------------------------------------------
pub use self::writer::save_meshregistry_definition;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use self::uuid::Uuid;
use std::collections::{BTreeMap, HashMap};

use yaml::parser::{MapParser, Parser, YamlType};
use yaml::Yaml;
use {Result, TryFromYaml, ValidatableElement};

use reader::{DatasetContainer, StatsOutput};

use model::primitives::{Rotation, Vector3D, GUID};
use model::rtti::CustomRttiDefinition;
use model::settings::QuestSettings;
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
impl Repository {
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.worlds.is_empty() && self.actions.is_empty() && self.journalgroups.is_empty()
    }
    // ------------------------------------------------------------------------
    pub fn worlds(&self) -> &HashMap<String, WorldInfo> {
        &self.worlds
    }
    // ------------------------------------------------------------------------
    pub fn actions(&self) -> &HashMap<String, ActionInfo> {
        &self.actions
    }
    // ------------------------------------------------------------------------
    pub fn foliage(&self) -> &HashMap<String, FoliageInfo> {
        &self.foliage
    }
    // ------------------------------------------------------------------------
    pub fn journalgroups(&self) -> &Vec<JournalGroup> {
        &self.journalgroups
    }
    // ------------------------------------------------------------------------
    pub fn entity_meshes(&self) -> &EntityMeshRegistry {
        &self.entity_meshes
    }
    // ------------------------------------------------------------------------
    pub fn custom_rtti(&self) -> &CustomRttiDefinition {
        &self.rtti
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalGroupInfo {
    // ------------------------------------------------------------------------
    fn new(name: &str) -> JournalGroupInfo {
        JournalGroupInfo {
            name: name.to_string(),
            guid: GUID::default(),
            path: String::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn new_questgroup(settings: &QuestSettings) -> JournalGroupInfo {
        let name = format!("questgroup_{}", settings.dlcid());
        JournalGroupInfo {
            path: settings.dirs().quest_journal_filepath(&name),
            name,
            guid: GUID::default(),
        }
    }
    // ------------------------------------------------------------------------
    fn set_guid(&mut self, guid: &str) -> Result<()> {
        match Uuid::parse_str(guid) {
            Ok(uuid) => {
                self.guid = GUID::from(&uuid);
                Ok(())
            }
            Err(_) => Err(format!(
                "uuid parser: could not parse uuid for [{}]: {}",
                self.name, guid
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn filepath(&self) -> &str {
        &self.path
    }
    // ------------------------------------------------------------------------
    pub fn name(&self) -> &str {
        &self.name
    }
    // ------------------------------------------------------------------------
    pub fn guid(&self) -> &GUID {
        &self.guid
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WorldInfo {
    // ------------------------------------------------------------------------
    pub fn worldid(&self) -> u8 {
        self.world_id
    }
    // ------------------------------------------------------------------------
    pub fn worldid_objective(&self) -> u8 {
        self.world_id + 1
    }
    // ------------------------------------------------------------------------
    pub fn world(&self) -> &str {
        &self.world_path
    }
    // ------------------------------------------------------------------------
    pub fn level_path(&self) -> &str {
        &self.level_path
    }
    // ------------------------------------------------------------------------
    pub fn navdata_path(&self) -> &str {
        &self.navdata_path
    }
    // ------------------------------------------------------------------------
    pub fn set_navdata_path<T: Into<String>>(&mut self, path: T) {
        self.navdata_path = path.into();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActionInfo {
    // ------------------------------------------------------------------------
    fn set_category(&mut self, category: &str) {
        self.category = category.to_string();
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::wrong_self_convention)]
    pub fn into_uid(category: &str, id: &str) -> String {
        format!("{}:{}", category, id).to_lowercase()
    }
    // ------------------------------------------------------------------------
    pub fn uid(&self) -> String {
        Self::into_uid(&self.category, &self.action_id)
    }
    // ------------------------------------------------------------------------
    pub fn ignore_collisions(&self) -> Option<bool> {
        self.ignore_collisions
    }
    // ------------------------------------------------------------------------
    pub fn file(&self) -> &String {
        &self.file
    }
    // ------------------------------------------------------------------------
    pub fn category(&self) -> &str {
        &self.category
    }
    // ------------------------------------------------------------------------
    pub fn place(&self) -> Option<&str> {
        if self.place.is_empty() {
            None
        } else {
            Some(&self.place)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl FoliageInfo {
    // ------------------------------------------------------------------------
    fn new(_id: &str) -> FoliageInfo {
        FoliageInfo {
            file: String::default(),
            has_collision: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_file(&mut self, filepath: &str) {
        let filepath = filepath.to_lowercase().replace('/', "\\");
        if self.has_collision.is_none() {
            self.has_collision = Some(filepath.contains("trees"));
        }
        self.file = filepath;
    }
    // ------------------------------------------------------------------------
    pub fn set_collision(&mut self, has: bool) {
        self.has_collision = Some(has);
    }
    // ------------------------------------------------------------------------
    pub fn file(&self) -> &String {
        &self.file
    }
    // ------------------------------------------------------------------------
    pub fn has_collision(&self) -> bool {
        self.has_collision.unwrap_or(false)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MeshComponentInfo {
    // ------------------------------------------------------------------------
    pub fn new<S: Into<String>>(
        mesh: S,
        pos: Vector3D,
        rot: Rotation,
        scale: Vector3D,
        ignore: bool,
    ) -> Self {
        MeshComponentInfo {
            mesh: mesh
                .into()
                .replace("\\\\", "/")
                .replace('\\', "/")
                .to_lowercase(),
            pos,
            rot,
            scale,
            ignore,
        }
    }
    // ------------------------------------------------------------------------
    pub fn mesh(&self) -> &str {
        &self.mesh
    }
    // ------------------------------------------------------------------------
    pub fn pos(&self) -> Vector3D {
        self.pos
    }
    // ------------------------------------------------------------------------
    pub fn rot(&self) -> Rotation {
        self.rot
    }
    // ------------------------------------------------------------------------
    pub fn scale(&self) -> Vector3D {
        self.scale
    }
    // ------------------------------------------------------------------------
    pub fn ignore(&self) -> bool {
        self.ignore
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EntityMeshRegistry {
    // ------------------------------------------------------------------------
    #[allow(clippy::map_entry)]
    pub fn add(&mut self, entitypath: &str, meshcomponents: Vec<MeshComponentInfo>) -> Result<()> {
        let path = entitypath
            .replace("\\\\", "/")
            .replace('\\', "/")
            .to_lowercase();

        if self.0.contains_key(&path) {
            Err(format!(
                "found duplicate entry for entity-meshes info: {}",
                path
            ))
        } else {
            self.0.insert(path, meshcomponents);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    pub fn get(&self, entitypath: &str) -> Option<&Vec<MeshComponentInfo>> {
        self.0.get(
            &entitypath
                .replace("\\\\", "/")
                .replace('\\', "/")
                .to_lowercase(),
        )
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    // ------------------------------------------------------------------------
    pub fn len(&self) -> usize {
        self.0.len()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalGroup {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match *self {
            JournalGroup::Character(ref data) | JournalGroup::Creature(ref data) => {
                data.validate(id)
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalGroupInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.path.is_empty() {
            Err(format!(
                "journal group [{}] requires non empty file path.",
                id
            ))
        } else if self.guid.uuid.is_nil() {
            Err(format!("journal group [{}] requires guid.", id))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for WorldInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.world_path.is_empty() || self.level_path.is_empty() || self.world_id == 0 {
            Err(format!(
                "world definition [{}] requires id, world and level settings.",
                id
            ))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ActionInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.file.is_empty() {
            Err(format!("action definition [{}] requires file setting.", id))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for FoliageInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.file.is_empty() {
            Err(format!(
                "foliage definition [{}] requires file setting.",
                id
            ))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for MeshComponentInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.mesh.is_empty() {
            return Err(format!(
                "meshcomponent definition [{}] requires mesh file setting.",
                id
            ));
        }
        if !self.mesh.ends_with(".w2mesh") {
            return Err(format!(
                "meshcomponent definition [{}] requires mesh reference file \
                extension \".w2mesh\". found: {}",
                id, self.mesh
            ));
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
