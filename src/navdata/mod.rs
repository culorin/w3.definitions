//
// definitions::navdata (yaml) definitions
//
// ----------------------------------------------------------------------------
mod reader;
// mod writer;

use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::navdata::{HubNavData, NavData, NavMesh};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl DatasetParser for NavData {
    type Dataset = Vec<(String, HubNavData)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(_context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found navdata definitions...");

        let mut dataset = Vec::new();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let id = id.to_lowercase();
            let navdataset = HubNavData::try_from_yaml(&id, &value)?;

            dataset.push((id, navdataset));
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for NavData {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if !data.is_empty() {
            debug!("> adding navdata definition...");
            for (id, dataset) in data {
                self.add_navdataset(id, dataset)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
