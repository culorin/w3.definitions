//
// navdata::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::{ListParser, MapParser, Parser, YamlType};

use model::primitives::Vector3D;

use {Result, TryFromYaml, ValidatableElement};

use super::{HubNavData, NavMesh};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for HubNavData {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found navdata definition for hub [{}]...", name);

        let mut navset = HubNavData::default();

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            let navmesh = match value.datatype() {
                YamlType::Array => NavMesh::try_from_yaml(key, &value.as_list()?)?,
                YamlType::Hash => NavMesh::try_from_yaml(key, &value.as_map()?)?,
                _ => return yaml_err!(
                    "expected type of {} to be a list (of triangle definition) or a key-value table.", name),
            };
            navset.add_navmesh(key, navmesh)?;
        }

        navset.validate(data.id()).and(Ok(navset))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<ListParser<'a>> for NavMesh {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &ListParser) -> Result<Self> {
        trace!(">> found navmesh [{}] definition...", name);

        let mut navmesh = NavMesh::new(name);

        for triangle in data.iter() {
            let vertices = triangle.as_list()?;
            let vertices = vertices.as_list_of_n_parser(3)?;

            navmesh.add_triangle(
                Vector3D::try_from_yaml((), &vertices[0])?,
                Vector3D::try_from_yaml((), &vertices[1])?,
                Vector3D::try_from_yaml((), &vertices[2])?,
            );
        }

        navmesh.validate(data.id()).and(Ok(navmesh))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<MapParser<'a>> for NavMesh {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &MapParser) -> Result<Self> {
        trace!(">> found navmesh [{}] definition...", name);

        let mut navmesh = NavMesh::new(name);

        for kvparser in data.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "triangles" => {
                    for triangle in value.as_list()?.iter() {
                        let vertices = triangle.as_list()?;
                        let vertices = vertices.as_list_of_n_parser(3)?;

                        navmesh.add_triangle(
                            Vector3D::try_from_yaml((), &vertices[0])?,
                            Vector3D::try_from_yaml((), &vertices[1])?,
                            Vector3D::try_from_yaml((), &vertices[2])?,
                        );
                    }
                }
                "mergeableborder" | "mergeableedges" => {
                    for edge in value.as_list()?.iter() {
                        let vertices = edge.as_list()?;
                        let vertices = vertices.as_list_of_n_parser(2)?;

                        navmesh.add_mergeable_edge(
                            Vector3D::try_from_yaml((), &vertices[0])?,
                            Vector3D::try_from_yaml((), &vertices[1])?,
                        );
                    }
                }
                // "_partitioning" => {}
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        navmesh.validate(data.id()).and(Ok(navmesh))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
