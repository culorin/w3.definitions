//
// definitions::shared
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use {IntoYaml, Result, TryFromYaml, ValidatableElement};
use yaml::Yaml;
use yaml::parser::{Parser, YamlType};
use yaml::encoder::{IntoYaml as EncoderIntoYaml, ListEncoder, MapEncoder};
use model::shared::{CompareFunction, FactsDbCondition, ScriptCall, ScriptParameter};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for FactsDbCondition {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        // [ moddlg_testfacts_havemet, "=", 1 ]

        let definition = data.as_list()?;
        let options = definition.as_parser_list();

        match options.len() {
            3 => Ok(FactsDbCondition::new(
                options[0].to_string()?,
                CompareFunction::try_from(options[1].to_lowercase_string()?.as_str())?,
                options[2].to_integer()?,
            )),
            l => yaml_err!(format!(
                "{}: condition list must contain 3 elements \
                 (factid, comparison operator, integer value). found: {}",
                data.id(),
                l
            )),
        }
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ScriptCall {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        // SCRIPT:
        //     function: my_function
        //     parameter:
        //         - stringparam: "string"
        //         - floatparam: 1.0
        //         - intparam: 1
        //         - boolparam: true
        let mut scriptcall = ScriptCall::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "function" => {
                    scriptcall.set_function(value.as_str()?);
                }
                "parameter" => {
                    let paramlist = value.as_list()?;

                    for parser in paramlist.iter() {
                        let map = parser.as_map()?;
                        let (name, value) = map.as_key_value()?;

                        scriptcall.add_param(ScriptParameter::try_from_yaml(name, &value)?);
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        scriptcall.validate(data.id()).and(Ok(scriptcall))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ScriptParameter {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, value: &Parser) -> Result<Self> {
        // parameter:
        //     - stringparam: "string"
        //     - floatparam: 1.0
        //     - intparam: 1
        //     - boolparam: true
        let scriptparam = match value.datatype() {
            YamlType::Real      => ScriptParameter::Float(name.into(), value.to_float()?),
            YamlType::Integer   => ScriptParameter::Int32(name.into(), value.to_integer()?),
            YamlType::Boolean   => ScriptParameter::Bool(name.into(), value.to_bool()?),
            YamlType::String    =>
                // unfortunately string and cname cannot be automatically detected
                // -> prefix usage
                match value.to_lowercase_string()?.as_str() {
                    t if t.starts_with("cname_")
                        // remove prefix
                        => ScriptParameter::CName(name.into(), value.as_str()?.split_at(6).1.into()),
                    _   => ScriptParameter::String(name.into(), value.to_string()?),
                },
            _ => return yaml_err!(value.id(), format!(
                    "supported types for function parameters are float, string, \
                    integer, boolean. found: {}", value.datatype_name())),
        };

        Ok(scriptparam)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for FactsDbCondition {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut list = ListEncoder::new_inlined();
        // [ moddlg_testfacts_havemet, "=", 1 ]

        list.add(self.fact());
        list.add(self.comparefunction().into_yaml());
        list.add(*self.value());

        list.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for CompareFunction {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::CompareFunction::*;

        match self {
            EQ => "=",
            NEQ => "!=",
            LT => "<",
            LTE => "<=",
            GT => ">",
            GTE => ">=",
        }.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ScriptCall {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut map = MapEncoder::new();
        // SCRIPT:
        //     function: my_function
        //     parameter:
        //         - stringparam: "string"
        //         - floatparam: 1.0
        //         - intparam: 1
        //         - boolparam: true
        map.add("function", self.function());
        map.add("parameter", self.params().into_yaml());

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ScriptParameter {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::ScriptParameter::*;
        // parameter:
        //     - stringparam: "string"
        //     - floatparam: 1.0
        //     - intparam: 1
        //     - boolparam: true
        let (name, value) = match self {
            Float(ref name, value) => (name, value.intoyaml()),
            Int32(ref name, value) => (name, value.intoyaml()),
            Bool(ref name, value) => (name, value.intoyaml()),
            // unfortunately string and cname cannot be automatically detected
            CName(ref name, ref value) => (name, Yaml::String(format!("cname_{}", value))),
            String(ref name, ref value) => (name, Yaml::String(value.to_owned())),
        };

        MapEncoder::from_key_value(name.as_str(), value).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
