//
// settings::quest
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use model::settings::{QuestDirectories, QuestSettings};

use super::{EncoderIntoYaml, IntoYaml, MapEncoder, Yaml};
use super::{Parser, TryFromYaml};
use {DatasetParser, Result, ValidatableElement};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
impl DatasetParser for QuestSettings {
    type Dataset = QuestSettings;
    // ------------------------------------------------------------------------
    fn parse_dataset<'data>(_context: Option<&str>, data: &Parser<'data>) -> Result<Self::Dataset> {
        info!("> found production definition...");

        let definition = data.as_map()?;

        let mut settings = yaml_err_not_found!(data.id(), "settings");

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "settings" => settings = Ok(QuestSettings::try_from_yaml((), &value)?),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        settings
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestSettings {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        info!("> found settings definition...");
        let mut settings = QuestSettings::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "id" => settings.set_dlcid(value.to_lowercase_string()?),
                "caption" => settings.set_caption(value.to_string()?),
                "description" => settings.set_description(value.to_string()?),
                "menuvisibile" => settings.set_menuvisibile(value.to_bool()?),
                "enabled" => settings.set_enabled(value.to_bool()?),
                "storesdata" => settings.set_storesdata(value.to_bool()?),
                "mounters" => settings.set_mounters(::rawdata_to_string(value.data())?),
                "strings-idspace" => settings.set_idspace(value.to_integer()? as u32),
                "strings-idstart" => settings.set_idstart(value.to_integer()? as u32),
                "directories" => settings.set_dirs(QuestDirectories::try_from_yaml((), &value)?),
                "version" => settings.set_version(value.to_integer()? as u32),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        settings
            .validate("settings")
            .and(settings.init().and(Ok(settings)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for QuestDirectories {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        trace!("> found directories definition...");
        let mut dirs = QuestDirectories::default();

        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "layerbase" => dirs.set_pref_layer(value.to_lowercase_string()?),
                "quest" => dirs.set_quest(value.to_lowercase_string()?),
                "phases" => dirs.set_phase(value.to_lowercase_string()?),
                "scenes" => dirs.set_scenes(value.to_lowercase_string()?),
                "communities" | "spawnsets" => dirs.set_spawnsets(value.to_lowercase_string()?),
                "templates" | "entities" => dirs.set_entities(value.to_lowercase_string()?),
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        dirs.validate("directories").and(Ok(dirs))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for QuestSettings {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        debug!("> serializing settings...");
        let mut map = MapEncoder::new();

        map.add("id", self.dlcid());
        map.add("caption", self.caption());
        map.add("description", self.description());
        map.add("menuvisibile", *self.menuvisibile());
        map.add("enabled", *self.enabled());
        map.add("storesdata", *self.storesdata());
        map.add("strings-idspace", *self.idspace());
        map.add("strings-idstart", *self.idstart());
        map.add("directories", self.dirs().into_yaml());
        map.add("version", *self.version());

        if let Some(mounters) = self.mounters() {
            let data = match ::rawdata_to_yaml(mounters) {
                Err(msg) => {
                    error!("failed to serialize mounters: {}", msg);
                    format!("failed to serialize mounters: {}", msg).into_yaml()
                }
                Ok(Some(data)) => data,
                Ok(None) => Yaml::Null,
            };
            map.add("mounters", data);
        }

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for QuestDirectories {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(">> serializing directories...");
        let mut map = MapEncoder::new();

        map.add("layerbase", self.pref_layer());

        let questdir = self.quest();
        let phasedir = self.phase().replacen(questdir, "", 1);

        // find base path to strip it from dirs
        let (basedir, questdir) = questdir.split_at(questdir.find("/data").map_or(0, |i| i + 5));
        let scenedir = self.scenes().replacen(basedir, "", 1);
        let spawnsetdir = self.spawnsets().replacen(basedir, "", 1);
        let entitydir = self.entities().replacen(basedir, "", 1);

        fn trimpath<'a, 'b: 'a>(path: &'b str) -> &'a str {
            if path.is_empty() {
                "/"
            } else {
                path.trim_start_matches('/')
            }
        }

        map.add("quest", questdir.trim_start_matches('/'));
        map.add("phases", phasedir.trim_start_matches('/'));

        // filter default values
        if scenedir != "/scenes" {
            map.add("scenes", trimpath(&scenedir));
        }
        if spawnsetdir != "/spawnsets" {
            map.add("communities", trimpath(&spawnsetdir));
        }
        if entitydir != "/entities" {
            map.add("templates", trimpath(&entitydir));
        }

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
