//
// community::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use yaml::parser::{ListParser, Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use model::primitives::references::{EntityReference, LayerTagReference};
use model::primitives::Time;

use super::{
    ActionPoint, Actor, ActorPhase, AddItems, AddTag, Community, DynamicWork, GuardArea, IdleAi,
    RainReaction, RiderAi, ScriptedInitializer, SetAppearance, SetAttitude, SetImmortality,
    SetLevel, SpawnSettings, WanderArea, WanderPath,
};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
fn add_actorphase(
    id: &str,
    community: &mut Community,
    actor: &str,
    phase: &str,
    phasedata: &Parser,
) -> Result<()> {
    if community.has_actor(&actor.to_lowercase()) {
        community.add_actorphase(
            phase,
            ActorPhase::try_from_yaml(actor, &(phase, phasedata))?,
        );
        Ok(())
    } else {
        return yaml_err!(
            id,
            format!(
                "found phase definition for undefined community actor: {}",
                actor
            )
        );
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Community {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        debug!("> found community [{}] definition...", name);
        let mut community = Community::new(name);

        // questgiver:
        //   entities:
        //     elderman: [...]
        //      ...
        //   phase:
        //     pre_quest: [..]
        //      ...

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "entities" | "actors" => {
                    for kvparser in value.as_map()?.iter() {
                        let (actor, data) = kvparser.to_key_value()?;

                        community.add_actor(Actor::try_from_yaml(actor, &data)?);
                    }
                }
                "phases" => {
                    for kvparser in value.as_map()?.iter() {
                        let (phase, data) = kvparser.to_key_value()?;

                        trace!("> found community phase definition [{}]...", phase);
                        // iterate over actors in phase definition
                        for kvparser in data.as_map()?.iter() {
                            let (actor, phasedata) = kvparser.to_key_value()?;

                            add_actorphase(data.id(), &mut community, actor, phase, &phasedata)?;
                        }
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        community.validate(data.id()).and(Ok(community))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Actor {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        trace!("> found actor [{}] definition...", name);
        let mut actor = Actor::new(name);

        // child1: "template.w2ent"
        // child2: [template, ["appearance1", "appearance2"], ["tag1", "tag2"]]
        // child3:
        //   template: "template.w2ent"
        //   appearances: ["a", "b", "c"]
        //   tags: ["a", "b"]

        match data.datatype() {
            String => actor.set_template(EntityReference::try_from_yaml((), data)?),
            Array => {
                let settings = data.as_list()?;
                let settings = settings.as_parser_list();

                if let Some(template_definition) = settings.get(0) {
                    actor.set_template(EntityReference::try_from_yaml((), template_definition)?);
                }
                if let Some(appearance_definition) = settings.get(1) {
                    actor.set_appearances(appearance_definition.to_list_of_lowercase_strings()?);
                }
                if let Some(tags_definition) = settings.get(2) {
                    actor.set_tags(tags_definition.to_list_of_lowercase_strings()?);
                }

                if settings.is_empty() || settings.len() > 3 {
                    return yaml_err!(data.id(),
                        format!("expected a 1-3 elements list (<template>, [<appearances>], [<tags>]). \
                        found: {} elements", settings.len()));
                }
            },
            Hash => {
                for kvparser in data.as_map()?.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.replace('_', "").to_lowercase().as_str() {
                        "template"    => actor.set_template(EntityReference::try_from_yaml((), &value)?),
                        "appearances" => actor.set_appearances(value.to_list_of_lowercase_strings()?),
                        "tags"        => actor.set_tags(value.to_list_of_lowercase_strings()?),
                        "guard"       => actor.add_initializer(GuardArea::try_from_yaml((), &value)?),
                        "reacttorain" => if value.to_bool()? {
                            actor.add_initializer(RainReaction)
                        }
                        _ => return yaml_err_unknown_key!(data.id(), key),
                    }
                };
            },
            _ => return yaml_err!(data.id(), format!(
                "supported types for community actor definition are string, list or key-value table. \
                found: {}", data.datatype_name())),
        }

        actor.validate(data.id()).and(Ok(actor))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a, 'id, 'data> TryFromYaml<(&'id str, &'data Parser<'a>)> for ActorPhase {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &(&str, &Parser)) -> Result<Self> {
        use self::YamlType::*;
        trace!("> found community phase definition for actor [{}]...", name);

        let mut phase = ActorPhase::new(name);

        // child1:
        //   "10:00":
        //     - [prologue/child_play, 4.0]  # actionpoint, weight
        //     - prologue/child_play2
        //   "12:00":
        //     - [prologue/child_eat_at_home, 4.0]
        //   "13:00": prologue/child_play
        // or:
        // child2:
        //   start_in_ap: true
        //   use_last_ap: true
        //   spawntimes:
        //     "10:00": [quantity, respawnDelay, respawn<bool>]
        //   actions:/actionpoints:
        //     "00:00": prologue/child_play
        // or:
        //  elderman: prologue/man_cry_for_help
        let (phaseid, data) = *data;

        match data.datatype() {
            String => phase.add_actionpoint(None, ActionPoint::try_from_yaml((), data)?),

            Hash => {
                // by scanning all keys decide if it's extended or actionpoint only mode
                let simple_definition = data.as_map()?.iter()
                    .map(|kvparser| kvparser.to_key_value().ok())
                    .map(|tuple| tuple.unwrap().0)
                    .any(|key| key.starts_with('0') || key.starts_with('1') || key.starts_with('2'));

                if simple_definition {
                    // only actionpoint list is valid at this point
                    parse_actionpoint_list(&mut phase, data)?;
                } else {
                    parse_extended_phase_list(phaseid, &mut phase, data)?;
                }
            },
            _ => return yaml_err!(data.id(), format!(
                "supported types for community actor phase definition are string or key-value table. \
                found: {}", data.datatype_name())),
        }

        phase.validate(data.id()).and(Ok(phase))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ActionPoint {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        Ok(ActionPoint::new(LayerTagReference::try_from_yaml(
            (),
            data,
        )?))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<ListParser<'a>> for ActionPoint {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &ListParser) -> Result<Self> {
        // - [prologue/child_play, 4.0]  # actionpoint, weight
        let settings = data.as_parser_list();

        if settings.len() == 2 {
            let mut ap = ActionPoint::try_from_yaml((), &settings[0])?;
            ap.set_weight(settings[1].to_float()?);
            Ok(ap)
        } else {
            yaml_err!(
                data.id(),
                format!(
                    "expected 2-element list (\"<world>/<actionpoint>\", <weight>). \
                     found: {}",
                    settings.len()
                )
            )
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn parse_actionpoint_list(phase: &mut ActorPhase, data: &Parser) -> Result<()> {
    use self::YamlType::*;
    //   "10:00":
    //     - [prologue/child_play, 4.0]  # actionpoint, weight
    //     - prologue/child_play2
    //   "12:00":
    //     - [prologue/child_eat_at_home, 4.0]
    //   "13:00": prologue/child_play

    for kvparser in data.as_map()?.iter() {
        let (time, value) = kvparser.to_key_value()?;

        let time = Time::try_from(time).map_err(|e| yaml_errmsg!(value.id(), e))?;
        let time = Some(&time);

        match value.datatype() {
            String => phase.add_actionpoint(time, ActionPoint::try_from_yaml((), &value)?),
            Array => {
                let ap_list = value.as_list()?;

                for value in ap_list.iter() {
                    match value.datatype() {
                        String => phase.add_actionpoint(time, ActionPoint::try_from_yaml((), &value)?),
                        Array => {
                            let ap = ActionPoint::try_from_yaml((), &value.as_list()?)?;
                            phase.add_actionpoint(time, ap);
                        },
                        _ => return yaml_err!(value.id(), format!(
                            "supported types for phase actionpoint definition are string or 2-element list \
                            (\"<world>/<actionpoint>\", <weight>). found: {}", value.datatype_name())),
                    }
                }
            }
            _ => {
                return yaml_err!(
                    value.id(),
                    format!(
                        "supported types for phase actionpoint definition are string or list \
                         (of actionpoints). found: {}",
                        value.datatype_name()
                    )
                )
            }
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn parse_spawntimes_list(phase: &mut ActorPhase, data: &Parser) -> Result<()> {
    //   "10:00": [quantity<u8>, respawn<bool>, respawnDelay<f32>]
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (time, value) = kvparser.to_key_value()?;

        let time = Time::try_from(time).map_err(|e| yaml_errmsg!(value.id(), e))?;
        phase.add_spawnsettings(time, SpawnSettings::try_from_yaml((), &value)?);
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn parse_extended_phase_list(phaseid: &str, phase: &mut ActorPhase, data: &Parser) -> Result<()> {
    let definition = data.as_map()?;
    trace!(">> found extended phase definition...");
    for kvparser in definition.iter() {
        let (key, value) = kvparser.to_key_value()?;

        match key.replace('_', "").to_lowercase().as_str() {
            "spawnat" => phase.add_spawnpoint(LayerTagReference::try_from_yaml((), &value)?),
            "despawnat" => phase.add_despawnpoint(LayerTagReference::try_from_yaml((), &value)?),
            "guard" => phase.add_decorator(GuardArea::try_from_yaml((), &value)?),
            "addtags" => phase.add_decorator(AddTag::try_from_yaml((), &value)?),
            "appearance" => phase.add_decorator(SetAppearance::try_from_yaml((), &value)?),
            "attitude" => phase.add_decorator(SetAttitude::try_from_yaml((), &value)?),
            "immortality" => phase.add_decorator(SetImmortality::try_from_yaml((), &value)?),
            "level" => phase.add_decorator(SetLevel::try_from_yaml((), &value)?),
            "additems" => phase.add_decorator(AddItems::try_from_yaml((), &value)?),
            "scripted" => {
                phase.add_decorator(ScriptedInitializer::new(&value.to_string()?, phaseid))
            }
            "dynamicwork" => phase.set_smart_ai(DynamicWork::try_from_yaml((), &value)?),
            "wanderpath" => phase.set_smart_ai(WanderPath::try_from_yaml((), &value)?),
            "wanderarea" => phase.set_smart_ai(WanderArea::try_from_yaml((), &value)?),
            "riderai" => {
                if value.to_bool()? {
                    phase.set_smart_ai(RiderAi::new(value.to_bool()?))
                } else {
                    return yaml_err!(
                        definition.id(),
                        "direct riderai deactivation not supported. see documentaion \
                         and use of idleai to overwrite behavior."
                    );
                }
            }
            "idleai" => phase.add_decorator(IdleAi::try_from_yaml(phaseid, &value)?),
            "startinap" => phase.set_start_in_ap(value.to_bool()?),
            "uselastap" => phase.set_use_last_ap(value.to_bool()?),
            "spawnhidden" => phase.set_spawn_hidden(value.to_bool()?),
            "spawntimes" => parse_spawntimes_list(phase, &value)?,
            "actions" | "actionpoints" => parse_actionpoint_list(phase, &value)?,

            _ => return yaml_err_unknown_key!(definition.id(), key),
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SpawnSettings {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        // [quantity<u8>, respawn<bool>, respawnDelay<f32>]
        let definition = data.as_list()?;
        let definition = definition.as_parser_list();

        match definition.len() {
            2 => {
                let mut settings = SpawnSettings::new(definition[0].to_integer()? as u8);
                settings.set_respawn(definition[1].to_bool()?);
                Ok(settings)
            }
            3 => {
                let mut settings = SpawnSettings::new(definition[0].to_integer()? as u8);
                settings.set_respawn(definition[1].to_bool()?);
                settings.set_respawn_delay(definition[2].to_integer()? as u32);
                Ok(settings)
            }
            _ => yaml_err!(
                data.id(),
                format!(
                    "expected 2-3 element list (<quantity>, <respawn>, <respawn delay>. \
                     found: {} elements",
                    definition.len()
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
