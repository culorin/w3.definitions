//
// community::spawntree - spawntree (yaml) definitions
//

// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use yaml::parser::{Parser, YamlType};
use {Result, TryFromYaml, ValidatableElement};

use model::primitives::enums::{ActorImmortality, MoveType};
use model::primitives::references::{ItemReference, LayerTagReference};

use model::community::spawntree::{
    AddItems, AddTag, DynamicWork, GuardArea, Pursuit, SetAppearance, SetAttitude, SetImmortality,
    SetLevel, WanderArea, WanderPath, IdleAi
};
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for GuardArea {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  guard: wolrd/guardarea
        //      or
        //  guard: [guardarea, pursuitarea]
        //      or
        //  guard: [guardarea, pursuitrange]

        let guardarea = match data.datatype() {
            String => GuardArea::new(
                Some(LayerTagReference::try_from_yaml((), data)?),
                Pursuit::None,
            ),
            Array => {
                let definition = data.as_list()?;
                let definition = definition.as_parser_list();

                if definition.len() == 2 {
                    GuardArea::new(
                        Some(LayerTagReference::try_from_yaml((), &definition[0])?),
                        Pursuit::try_from_yaml((), &definition[1])?,
                    )
                } else {
                    return yaml_err!(
                        data.id(),
                        format!(
                            "expected 2 element list (<guard-area>, <pursuit-area> or \
                             <pursuitrange>). found: {} elements",
                            definition.len()
                        )
                    );
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for guard area definition are string or \
                         list of values. found: {}",
                        data.datatype_name()
                    )
                )
            }
        };
        guardarea.validate(data.id()).and(Ok(guardarea))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Pursuit {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        let pursuit = match data.datatype() {
            String => Pursuit::Area(LayerTagReference::try_from_yaml((), data)?),
            Real => Pursuit::Range(data.to_float()?),
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported type for pursuit definition is string (area reference) \
                         or float (pursuitrange). found: {}",
                        data.datatype_name()
                    )
                )
            }
        };
        pursuit.validate(data.id()).and(Ok(pursuit))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for AddTag {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  tags: tag1
        //      or
        //  tags: [tag1, tag2]
        //      or
        //  tags: [tag1, tag2, only_on_spawn_bool]

        let decorator = match data.datatype() {
            String => AddTag::new(vec![data.to_lowercase_string()?]),
            Array => {
                let definition = data.as_list()?;
                let mut definition = definition.as_parser_list();

                let last = if definition.len() > 1 {
                    definition.split_off(definition.len() - 1)
                } else {
                    Vec::default()
                };
                let mut tags = definition
                    .iter()
                    .map(Parser::to_lowercase_string)
                    .collect::<Result<Vec<_>>>()?;

                if let Some(last) = last.first() {
                    match last.datatype() {
                        Boolean => {
                            let mut decorator = AddTag::new(tags);
                            decorator.set_on_spawn_only(last.to_bool()?);
                            decorator
                        }
                        _ => {
                            tags.push(last.to_lowercase_string()?);
                            AddTag::new(tags)
                        }
                    }
                } else {
                    AddTag::new(tags)
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                    "supported types for tags definition are string or list of values. found: {}",
                    data.datatype_name()
                )
                )
            }
        };
        decorator.validate(data.id()).and(Ok(decorator))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SetAppearance {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  appearance: someappearance
        //      or
        //  appearance: [someappearance, only_on_spawn_bool]

        let decorator = match data.datatype() {
            String => SetAppearance::new(data.to_lowercase_string()?),
            Array => {
                let definition = data.as_list()?;
                let definition = definition.as_parser_list();

                if definition.len() == 2 {
                    let mut decorator = SetAppearance::new(definition[0].to_lowercase_string()?);
                    decorator.set_on_spawn_only(definition[1].to_bool()?);
                    decorator

                } else {
                    return yaml_err!(
                        data.id(),
                        format!(
                            "expected 2 element list (<appearance-name>, <on-spawn-only-bool>). found: {} elements",
                            definition.len()
                        )
                    );
                }
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for appearance definition are string or list of values. found: {}",
                        data.datatype_name()
                    )
                )
            }
        };
        decorator.validate(data.id()).and(Ok(decorator))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SetAttitude {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        //  attitude: someattitude

        let decorator = SetAttitude::new(data.to_string()?);
        decorator.validate(data.id()).and(Ok(decorator))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SetImmortality {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        //  immortality: somemode

        let decorator = SetImmortality::new(ActorImmortality::try_from(data.as_str()?)?);
        decorator.validate(data.id()).and(Ok(decorator))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SetLevel {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        //  level: 123

        let level = data.to_integer()?;
        if level > 0 && level <= 200 {
            let decorator = SetLevel::new(level as u8);
            decorator.validate(data.id()).and(Ok(decorator))
        } else {
            Err(format!("supported level must be [1..200]. found {}", level))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for AddItems {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  addItems:
        //    - [someitem/item, 2]
        //    - someitem/item
        // or
        //  addItems:
        //    equip_first: true
        //    one_random: true
        //    items:
        //      - [someitem/item, 2]
        //      - someitem/item

        let read_items = |decorator: &mut AddItems, data: &Parser| -> Result<()> {
            for value in data.as_list()?.iter() {
                let (item, quantity) = match value.datatype() {
                    String => (ItemReference::try_from_yaml((), &value)?, 1),
                    Array => {
                        let def = value.as_list()?;
                        let def = def.as_list_of_n_parser(2)?;
                        (
                            ItemReference::try_from_yaml((), &def[0])?,
                            def[1].to_integer()? as u32,
                        )
                    }
                    _ => {
                        return yaml_err!(
                            value.id(),
                            format!(
                            "supported types for item definition are string (<item-reference>) \
                            or list (<item-reference>, <quantity>). found: {}",
                            value.datatype_name()
                        )
                        )
                    }
                };

                decorator.add_item((item, quantity));
            }
            Ok(())
        };

        let mut decorator = AddItems::default();

        match data.datatype() {
            Hash => {
                let definition = data.as_map()?;
                trace!(">> found extended additems decorator...");

                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.replace('_', "").to_lowercase().as_str() {
                        "equipfirst" => decorator.set_equip_first(value.to_bool()?),
                        "onerandom" => decorator.set_one_random(value.to_bool()?),
                        "items" => read_items(&mut decorator, &value)?,
                        _ => return yaml_err_unknown_key!(definition.id(), key),
                    }
                }
            }
            Array => read_items(&mut decorator, data)?,
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for additems definition are list (of items) or \
                         key value table. found: {}",
                        data.datatype_name()
                    )
                )
            }
        }

        decorator.validate(data.id()).and(Ok(decorator))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for DynamicWork {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        //  dynamicwork:
        //    movetype: walk|run|..
        //    actionpoint_tags:
        //      - ap1
        //      - ap2
        //    categories:
        //      - cat1
        //      - cat2
        //    restrict_to_area: prologue/area
        //    keep_aps: false

        let mut smartai = DynamicWork::default();

        let definition = data.as_map()?;
        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.replace('_', "").to_lowercase().as_str() {
                "movetype" => smartai.set_movetype(MoveType::try_from(value.as_str()?)?),
                "actionpointtags" => smartai.set_aptags(value.to_list_of_lowercase_strings()?),
                "restricttoarea" => {
                    smartai.set_aparea(LayerTagReference::try_from_yaml((), &value)?)
                }
                "categories" => smartai.set_categories(value.to_list_of_lowercase_strings()?),
                "keepaps" => smartai.set_keep_aps(value.to_bool()?),
                _ => return yaml_err_unknown_key!(definition.id(), key),
            }
        }
        smartai.validate(data.id()).and(Ok(smartai))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WanderPath {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  wanderpath: <reference to wanderpointgroup>
        //  wanderpath:
        //      speed: 1.0
        //      type: walk|run|..
        //      wanderpoints: <reference to group>
        //      maxdistance: 10.0
        //      rightside

        let smartai = match data.datatype() {
            String => WanderPath::new(LayerTagReference::try_from_yaml((), data)?),
            Hash => {
                let definition = data.as_map()?;
                trace!(">> found extended wanderpath definition...");

                let mut smartai = WanderPath::default();
                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.replace('_', "").to_lowercase().as_str() {
                        "speed" => smartai.set_speed(value.to_float()?),
                        "movetype" => smartai.set_movetype(MoveType::try_from(value.as_str()?)?),
                        "maxdistance" => smartai.set_maxdistance(value.to_float()?),
                        "wanderpoints" => smartai
                            .set_wanderpoint_group(LayerTagReference::try_from_yaml((), &value)?),
                        "rightside" => smartai.set_rightside(value.to_bool()?),
                        _ => return yaml_err_unknown_key!(definition.id(), key),
                    }
                }
                smartai
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for wanderpath definition are string or \
                         key value table. found: {}",
                        data.datatype_name()
                    )
                )
            }
        };
        smartai.validate(data.id()).and(Ok(smartai))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for WanderArea {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::YamlType::*;

        //  wanderarea: <reference to area>
        //  wanderarea:
        //      area: <reference to area>
        //      speed: 1.0
        //      type: walk|run|..
        //      maxdistance: 10.0
        //      mindistance: 10.0
        //      idleDuration: 10.0
        //      idleChance: 1.0
        //      moveDuration: 10.0
        //      moveChance: 1.0
        //      useGuardArea: 1.0

        let smartai = match data.datatype() {
            String => WanderArea::new(LayerTagReference::try_from_yaml((), data)?),
            Hash => {
                let definition = data.as_map()?;
                trace!(">> found extended wanderarea definition...");

                let mut smartai = WanderArea::default();
                for kvparser in definition.iter() {
                    let (key, value) = kvparser.to_key_value()?;

                    match key.replace('_', "").to_lowercase().as_str() {
                        "speed" => smartai.set_speed(value.to_float()?),
                        "movetype" => smartai.set_movetype(MoveType::try_from(value.as_str()?)?),
                        "maxdistance" => smartai.set_maxdistance(value.to_float()?),
                        "mindistance" => smartai.set_mindistance(value.to_float()?),
                        "idleduration" => smartai.set_idle_duration(value.to_float()?),
                        "moveduration" => smartai.set_move_duration(value.to_float()?),
                        "idlechance" => smartai.set_idle_chance(value.to_float()?),
                        "movechance" => smartai.set_move_chance(value.to_float()?),
                        "area" => smartai.set_area(LayerTagReference::try_from_yaml((), &value)?),
                        "useguardarea" => smartai.set_use_guardarea(value.to_bool()?),
                        _ => return yaml_err_unknown_key!(definition.id(), key),
                    }
                }
                smartai
            }
            _ => {
                return yaml_err!(
                    data.id(),
                    format!(
                        "supported types for wanderarea definition are string or \
                         key value table. found: {}",
                        data.datatype_name()
                    )
                )
            }
        };
        smartai.validate(data.id()).and(Ok(smartai))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for IdleAi {
    type Id = &'data str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(
        name: &str,
        data: &Parser<'data>,
    ) -> Result<Self> {

        // e.g.
        // idleAI:
        //   class: CSpawnTreeInitializerSmartWanderAI
        //   treename: ...  # some treename, default: idleTree, riderIdleTree, freeFlight, startingBehavier
        //   idletree:
        //     .type: CAIWanderWithHistory
        //     wanderMoveType: run
        //     params:
        //         .type: CAINpcHistoryWanderParams
        //         wanderPointsGroupTag: prologue/spawn_point
        //         rightSideMovement: true

        debug!("> found idle ai decorator for [{}] actor...", name);

        let map = data.as_map()?;
        let aiclass = map.get_value("class")
            .ok_or_else(|| yaml_errmsg!(data.id(), "required \"class\" setting not found."))??
            .to_string()?;

        map.get_value("idletree")
            .ok_or_else(|| yaml_errmsg!(data.id(), "required \"idletree\" setting not found."))??;

        for kvparser in map.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "class" => { },
                "treename" | "dynamictreeparametername" => {
                    value.to_string()?;
                }
                "idletree" => {
                    // ommitted generic class reader
                }
                _ => return yaml_err_unknown_key!(map.id(), key),
            }
        }
        let idle_ai = IdleAi::new(&aiclass);

        idle_ai.validate(data.id()).and(Ok(idle_ai))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
