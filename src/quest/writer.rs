//
// definitions::quest::writer
//
// ----------------------------------------------------------------------------
use std::path::Path;

use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};
use yaml::Yaml;

use writer::{DataSet, DatasetProvider};

use model::questgraph::segments;
use model::QuestDefinition;

use {IntoYaml, Result};
// ----------------------------------------------------------------------------
trait DefinitionToYaml {
    fn production_to_yaml(&self) -> Yaml;
    fn structure_root_to_yaml(root: &segments::QuestRootSegment) -> Yaml;
    fn structure_to_yaml(segment: &segments::QuestSegment) -> Yaml;
}
// ----------------------------------------------------------------------------
impl DefinitionToYaml for QuestDefinition {
    // ------------------------------------------------------------------------
    fn production_to_yaml(&self) -> Yaml {
        let mut production = MapEncoder::new();

        if let Some(settings) = self.settings() {
            production.add("settings", settings.into_yaml());
        }

        MapEncoder::from_key_value("production", production).intoyaml()
    }
    // ------------------------------------------------------------------------
    fn structure_root_to_yaml(segment: &segments::QuestRootSegment) -> Yaml {
        let structure = MapEncoder::from_key_value("quest", segment.into_yaml());

        MapEncoder::from_key_value("structure", structure).intoyaml()
    }
    // ------------------------------------------------------------------------
    fn structure_to_yaml(segment: &segments::QuestSegment) -> Yaml {
        let structure = MapEncoder::from_key_value(
            "segments",
            MapEncoder::from_key_value(&*segment.id().as_str(), segment.into_yaml()),
        );

        MapEncoder::from_key_value("structure", structure).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetProvider for QuestDefinition {
    // ------------------------------------------------------------------------
    fn as_datasets(&self, _outpath: &Path) -> Result<Vec<DataSet>> {
        use writer::DataSet::*;

        // extract version and stamp it into every file
        let (questid, ver) = self
            .settings()
            .as_ref()
            .map(|s| (s.dlcid().to_lowercase(), *s.version()))
            .ok_or("missing settings")?;

        // -- production
        let mut sets = vec![Production(ver, questid, self.production_to_yaml())];

        // -- structure
        let root = self.structure().root()?;

        sets.push(StructureRoot(
            ver,
            "root".into(),
            Self::structure_root_to_yaml(root),
        ));

        for segment in self.structure().segments() {
            sets.push(StructureSegment(
                ver,
                if segment.context().is_empty() {
                    None
                } else {
                    Some(segment.context().to_string())
                },
                segment.id().to_string(),
                Self::structure_to_yaml(segment),
            ));
        }

        // -- rawdata
        for (filename, rawdata) in self.rawdata() {
            sets.push(RawData(filename.to_owned(), rawdata.to_owned()));
        }

        Ok(sets)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
