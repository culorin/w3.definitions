//
// definitions::primitives
//

// ----------------------------------------------------------------------------
mod references;
// ----------------------------------------------------------------------------
use yaml::encoder::{ListEncoder, MapEncoder};
use yaml::parser::{ListParser, MapParser, Parser, YamlType};
use yaml::Yaml;
use {IntoYaml, Result, TryFromYaml};

use model::primitives::{
    DateTime, HiResTime, RotatedPosition, Rotation, Time, Transform3D, Vector3D,
};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for DateTime {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        // use chrono::prelude::*;
        use chrono::prelude::DateTime as ChronoDateTime;
        use chrono::{Datelike, Timelike};

        // let datetime = data.as_str()?;

        let dt = ChronoDateTime::parse_from_rfc3339(data.as_str()?)
            .map_err(|e| format!("failed to parse datetime: {}", e))?;

        Ok(DateTime {
            date: (dt.year() as u16, dt.month() as u8, dt.day() as u8),
            time: (
                dt.hour() as u8,
                dt.minute() as u8,
                dt.second() as u8,
                dt.nanosecond() as u32,
            ),
        })
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Vector3D {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let list = data.to_list_of_n_floats(3)?;

        Ok(Vector3D(list[0], list[1], list[2]))
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Rotation {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let list = data.to_list_of_n_floats(3)?;

        Ok(Rotation(list[0], list[1], list[2]))
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<ListParser<'a>> for RotatedPosition {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &ListParser) -> Result<Self> {
        let list = data.as_parser_list();

        match list.len() {
            3 => Ok(RotatedPosition(
                Vector3D(
                    list[0].to_float()?,
                    list[1].to_float()?,
                    list[2].to_float()?,
                ),
                Rotation(0.0, 0.0, 0.0),
            )),
            4 => Ok(RotatedPosition(
                Vector3D(
                    list[0].to_float()?,
                    list[1].to_float()?,
                    list[2].to_float()?,
                ),
                // only roll supported
                Rotation(0.0, 0.0, list[3].to_float()?),
            )),
            6 => Ok(RotatedPosition(
                Vector3D(
                    list[0].to_float()?,
                    list[1].to_float()?,
                    list[2].to_float()?,
                ),
                Rotation(
                    list[3].to_float()?,
                    list[4].to_float()?,
                    list[5].to_float()?,
                ),
            )),
            _ => yaml_err!(
                data.id(),
                format!(
                    "expected a 3, 4 or 6 elements list (x, y, z, optional \
                     z or optional x/y/z rotation). found: {} elements",
                    list.len()
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<MapParser<'a>> for RotatedPosition {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &MapParser) -> Result<Self> {
        let mut pos = Vector3D::default();
        let mut rot = Rotation::default();

        for kvparser in data.iter() {
            let (key, value) = kvparser.to_key_value()?;
            match key.to_lowercase().as_str() {
                "pos" => pos = Vector3D::try_from_yaml((), &value)?,
                "rot" => rot = Rotation::try_from_yaml((), &value)?,

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(RotatedPosition(pos, rot))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for RotatedPosition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(errname: &str, data: &Parser) -> Result<Self> {
        match data.datatype() {
            YamlType::Array => RotatedPosition::try_from_yaml((), &data.as_list()?),
            YamlType::Hash => RotatedPosition::try_from_yaml((), &data.as_map()?),

            _ => yaml_err!(
                data.id(),
                format!(
                    "supported types for {} definition are list or key-value table. \
                     found: {}",
                    errname,
                    data.datatype_name()
                )
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for Transform3D {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        let mut pos = Vector3D::default();
        let mut rot = Rotation::default();
        let mut scale = Vector3D::default();

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;
            match key.to_lowercase().as_str() {
                "pos" => pos = Vector3D::try_from_yaml((), &value)?,
                "rot" => rot = Rotation::try_from_yaml((), &value)?,
                "scale" => scale = Vector3D::try_from_yaml((), &value)?,

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(Transform3D { pos, rot, scale })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for String {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        if self.is_empty() {
            Yaml::Null
        } else {
            Yaml::String(self.to_owned())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Time {
    type Output = Yaml;

    fn into_yaml(&self) -> Self::Output {
        Yaml::String(format!("{}", self))
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for HiResTime {
    type Output = Yaml;

    fn into_yaml(&self) -> Self::Output {
        Yaml::String(format!("{}", self))
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for DateTime {
    type Output = Yaml;

    fn into_yaml(&self) -> Self::Output {
        use chrono::prelude::*;

        trace!(">> datetime: {:?}", self);
        let datetime = Utc
            .ymd(self.date.0.into(), self.date.1.into(), self.date.2.into())
            .and_hms_milli(
                self.time.0.into(),
                self.time.1.into(),
                self.time.2.into(),
                self.time.3,
            );

        Yaml::String(datetime.to_rfc3339())
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for Vector3D {
    type Output = Yaml;

    fn into_yaml(&self) -> Self::Output {
        use yaml::encoder::IntoYaml;

        let mut list = ListEncoder::new_inlined();

        list.add(self.0);
        list.add(self.1);
        list.add(self.2);

        list.intoyaml()
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for Rotation {
    type Output = Yaml;

    fn into_yaml(&self) -> Self::Output {
        use yaml::encoder::IntoYaml;

        let mut list = ListEncoder::new_inlined();

        list.add(self.0);
        list.add(self.1);
        list.add(self.2);

        list.intoyaml()
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for Transform3D {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use yaml::encoder::IntoYaml;

        let mut map = MapEncoder::new();

        map.add("pos", self.pos.into_yaml());
        if self.rot != Rotation(0.0, 0.0, 0.0) {
            map.add("rot", self.rot.into_yaml());
        }
        if self.scale != Vector3D(0.0, 0.0, 0.0) {
            map.add("scale", self.scale.into_yaml());
        }
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// generic trait implementations
// ----------------------------------------------------------------------------
impl<'a, T> IntoYaml for Option<&'a T>
where
    T: IntoYaml<Output = Yaml>,
{
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        match self {
            Some(data) => data.into_yaml(),
            None => Yaml::Null,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T> IntoYaml for Vec<T>
where
    T: IntoYaml<Output = Yaml>,
{
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use yaml::encoder::IntoYaml;

        if self.is_empty() {
            Yaml::Null
        } else {
            let mut list = ListEncoder::new();
            for element in self {
                list.add(element.into_yaml());
            }
            list.intoyaml()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
