//
// primitives::references
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use model::primitives::references::{
    ActorReference, EntityReference, InteractionReference, ItemReference, LayerTagReference,
    LootReference, RewardReference, SceneReference,
};
use yaml::parser::Parser;
use yaml::Yaml;
use {IntoYaml, Result, TryFromYaml};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LayerTagReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => {
                Ok(LayerTagReference::UncheckedTag(s.split_at(1).1.to_owned()))
            }
            id => {
                let parts: Vec<&str> = id.split('/').collect();

                match parts.len() {
                    2 => Ok(LayerTagReference::new(parts[0], parts[1])),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a two part declaration \
                             (<world>/<id>). found: {}",
                            id
                        )
                    ),
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFromYaml<(Option<String>, String)> for LayerTagReference {
    type Id = String;
    // ------------------------------------------------------------------------
    fn try_from_yaml(dataid: String, data: &(Option<String>, String)) -> Result<Self> {
        let (ref world, ref reference) = *data;

        match *world {
            Some(ref world) => Ok(LayerTagReference::new(world, reference)),
            None => yaml_err!(dataid, "expected world reference not found".to_string()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ActorReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => Ok(ActorReference::Unchecked(s.split_at(1).1.to_owned())),

            id => {
                let parts: Vec<&str> = id.split('/').collect();

                match parts.len() {
                    2 => Ok(ActorReference::CommunityActor(
                        parts[0].to_lowercase(),
                        parts[1].to_lowercase(),
                    )),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a two part declaration \
                             (<community>/<actor>). found: {}",
                            id
                        )
                    ),
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for EntityReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.ends_with(".w2ent") => {
                let parts: Vec<&str> = s.split(':').collect();

                match parts.len() {
                    1 => Ok(EntityReference::Unchecked(escape_path(s))),
                    2 => Ok(EntityReference::ClassAndReference(
                        parts[0].to_string(),
                        escape_path(parts[1]),
                    )),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a two part declaration \
                             (<entityclass>:<templatepath>). found: {}",
                            s
                        )
                    ),
                }
            }
            id => {
                let parts: Vec<&str> = id.split(':').collect();

                match parts.len() {
                    2 => Ok(EntityReference::ClassAndId(
                        parts[0].to_string(),
                        parts[1].to_lowercase(),
                    )),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a two part declaration \
                             (<entityclass>:<templateid>). found: {}",
                            id
                        )
                    ),
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for InteractionReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => {
                Ok(InteractionReference::Unchecked(s.split_at(1).1.to_owned()))
            }
            id => {
                let parts: Vec<&str> = id.split('/').collect();

                match parts.len() {
                    2 => Ok(InteractionReference::new_entity(parts[0], parts[1])),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a either a two part declaration \
                             (<world>/<id>) for interactive layer entity. found: {}",
                            id
                        )
                    ),
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for SceneReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        // scene: "scene_no_extension"                         // will generate path
        // scene: "scenepatch/scene_with_extension.yml"        // will generate path
        // scene: "scenepatch/scene_with_extension.w2scene"    // will NOT generate path, takes as it is, no checks

        // normalized to path/name without extension
        match data.as_str()? {
            s if s.starts_with('/') || s.starts_with('\\') => {
                if s.ends_with(".w2scene") {
                    Ok(SceneReference::Unchecked(escape_path(s)))
                } else {
                    yaml_err!(
                        data.id(),
                        format!(
                            "absolute path reference must end with .w2scene. found: {}",
                            s
                        )
                    )
                }
            }
            s if s.ends_with(".w2scene") => Ok(SceneReference::Unchecked(escape_path(s))),
            s if s.ends_with(".yml") => Ok(SceneReference::Definition(
                s.split_at(s.len() - 4).0.to_lowercase(),
            )),
            s => Ok(SceneReference::Definition(s.to_lowercase())),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ItemReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => Ok(ItemReference::Unchecked(s.split_at(1).1.to_owned())),

            id => {
                let parts: Vec<&str> = id.split('/').collect();

                match parts.len() {
                    2 => Ok(ItemReference::CategoryAndName(
                        parts[0].to_lowercase(),
                        parts[1].to_lowercase(),
                    )),
                    _ => yaml_err!(
                        data.id(),
                        format!(
                            "reference must be a two part declaration \
                             (<category>/<itemname>). found: {}",
                            id
                        )
                    ),
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LootReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => Ok(LootReference::Unchecked(s.split_at(1).1.to_owned())),

            id => Ok(LootReference::Id(id.to_lowercase())),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for RewardReference {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        match data.as_str()? {
            s if s.starts_with('~') => Ok(RewardReference::Unchecked(s.split_at(1).1.to_owned())),

            id => Ok(RewardReference::Id(id.to_lowercase())),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn escape_path(path: &str) -> String {
    path.replace('\\', "/").replace("//", "/").to_lowercase()
}
// ----------------------------------------------------------------------------
// IntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for LayerTagReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::LayerTagReference::*;

        match self {
            UncheckedTag(tag) => Yaml::String(format!("~{}", tag)),
            WorldAndId(world, id) => Yaml::String(format!("{}/{}", world, id)),
            WorldAndTag(world, tag) => Yaml::String(format!("{}/{}", world, tag)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ActorReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::ActorReference::*;

        match self {
            Unchecked(tag) => Yaml::String(format!("~{}", tag)),
            CommunityActor(community, actorid) => {
                Yaml::String(format!("{}/{}", community, actorid))
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for EntityReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::EntityReference::*;

        match self {
            Unchecked(value) => Yaml::String(value.to_owned()),
            ClassAndReference(class, value) => Yaml::String(format!("{}:{}", class, value)),
            ClassAndId(class, id) => Yaml::String(format!("{}:{}", class, id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for InteractionReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::InteractionReference::*;

        match self {
            Unchecked(tag) => Yaml::String(format!("~{}", tag)),
            LayerEntity(world, id) => Yaml::String(format!("{}/{}", world, id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for SceneReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::SceneReference::*;

        match self {
            Definition(scene_def) => Yaml::String(format!("{}.yml", scene_def)),
            Unchecked(path) => Yaml::String(path.to_owned()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ItemReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::ItemReference::*;

        match self {
            CategoryAndName(cat, name) => Yaml::String(format!("{}/{}", cat, name)),
            Unchecked(id) => Yaml::String(format!("~{}", id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for LootReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::LootReference::*;

        match self {
            Id(id) => Yaml::String(id.to_owned()),
            Unchecked(id) => Yaml::String(format!("~{}", id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for RewardReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::RewardReference::*;

        match self {
            Id(id) => Yaml::String(id.to_owned()),
            Unchecked(id) => Yaml::String(format!("~{}", id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
