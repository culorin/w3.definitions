//
// definitions::rtti - custom rtti (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use yaml::parser::Parser;
use {Result, TryFromYaml, ValidatableElement};

use model::rtti::{
    AssetReferenceRttiType, AttributeRttiType, ClassRttiDefinition, CustomRttiDefinition,
    PrimitiveRttiType,
};
// ----------------------------------------------------------------------------
/// local helper trait
trait RttiParser {
    fn parse_classes(&mut self, src: &str, data: &Parser) -> Result<()>;
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for CustomRttiDefinition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(src: &str, data: &Parser) -> Result<Self> {
        let definition = data.as_map()?;

        let mut def = CustomRttiDefinition::default();

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "classes" => def.parse_classes(src, &value)?,
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(def)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl RttiParser for CustomRttiDefinition {
    // ------------------------------------------------------------------------
    fn parse_classes(&mut self, src: &str, data: &Parser) -> Result<()> {
        debug!("> {}: found rtti class definitons...", src);
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (class, value) = kvparser.to_key_value()?;
            self.add_class(class, ClassRttiDefinition::try_from_yaml(class, &value)?)?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ClassRttiDefinition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found rtti for class [{}]...", name);
        // CEntity:
        //   .extends: CSomething
        //   .adds:
        //      ...
        let mut rtti = ClassRttiDefinition::new(name);
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                ".extends" if value.to_lowercase_string()?.as_str() == "none" => {
                    // ignore empty baseclass to allow creation of enums (do
                    // not have attributes or baseclass -> empty definition)
                }
                ".extends" => rtti.set_baseclass(value.as_str()?),
                ".adds" => {
                    let attributes = value.as_map()?;
                    for kvparser in attributes.iter() {
                        let (name, value) = kvparser.to_key_value()?;
                        rtti.add_attribute(name, AttributeRttiType::try_from_yaml((), &value)?)?;
                    }
                }
                _ => return yaml_err_unknown_key!(value.id(), key),
            }
        }
        rtti.validate(data.id()).and(Ok(rtti))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for AttributeRttiType {
    type Id = ();
    // ------------------------------------------------------------------------
    fn try_from_yaml(_: (), data: &Parser) -> Result<Self> {
        use self::AssetReferenceRttiType::Area;
        use self::AttributeRttiType::*;
        use self::PrimitiveRttiType::*;
        // .adds:
        //   newBool: bool
        //   newInt: int
        //   newName: cname|name
        //   newTypedRef: cname.area|wanderpoint|waypoint|actionpoint|mappin|item|loot
        //   newString: String
        //   newFloat: Float
        //   newTagList: TagList
        //   newGuid: Guid
        //   newEntityHandle: entityhandle
        //   newTypedEntityHandle: entityhandle.area
        //   newEnum: enum<ESomeEnumName>
        //   newArray: array<CSomeClass>
        //   newPrimitivesArray: array<bool>
        //   newTypedRefArray: array<cname.area|...>
        let key = data.as_str()?;
        let attrtype = match key.to_lowercase().as_str() {
            "bool" => Primitive(Bool),
            "int" => Primitive(Int),
            "name" | "cname" => Primitive(Name),
            t if t.starts_with("cname.") | t.starts_with("name.") => {
                let reftype = t.split_at(key.find('.').unwrap() + 1).1;
                let reftype = AssetReferenceRttiType::try_from(reftype)
                    .map_err(|e| yaml_errmsg!(data.id(), e))?;

                Primitive(NameRef(reftype))
            }
            "string" => Primitive(String),
            "float" => Primitive(Float),
            "taglist" => Primitive(TagList),
            "guid" => Primitive(Guid),
            "entityhandle" => Primitive(EntityHandle),
            "entityhandle.area" => Primitive(EntityHandleRef(Area)),
            t if t.starts_with("enum<") && t.ends_with('>') => {
                Enum(key.get(5..key.len() - 1).unwrap().trim().to_owned())
            }
            t if t.starts_with("array<") && t.ends_with('>') => {
                let array_type = key.get(6..key.len() - 1).unwrap().trim();
                match array_type.to_lowercase().as_str() {
                    "bool" => ArrayPrimitives(Bool),
                    "int" => ArrayPrimitives(Int),
                    "name" | "cname" => ArrayPrimitives(Name),
                    t if t.starts_with("cname.") || t.starts_with("name.") => {
                        let reftype = t.split_at(array_type.find('.').unwrap() + 1).1;
                        let reftype = AssetReferenceRttiType::try_from(reftype)
                            .map_err(|e| yaml_errmsg!(data.id(), e))?;

                        ArrayPrimitives(NameRef(reftype))
                    }
                    "string" => ArrayPrimitives(String),
                    "float" => ArrayPrimitives(Float),
                    "taglist" => ArrayPrimitives(TagList),
                    "guid" => ArrayPrimitives(Guid),
                    "entityhandle" => ArrayPrimitives(EntityHandle),
                    _ => ArrayHandles(array_type.to_string()),
                }
            }
            _ => Handle(key.to_string()),
        };
        attrtype.validate(data.id()).and(Ok(attrtype))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
